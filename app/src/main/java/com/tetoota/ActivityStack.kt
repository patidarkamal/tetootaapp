package com.tetoota
import android.content.Context
import android.support.v7.app.AppCompatActivity
import java.util.*

/**
 * Created by charchit.kasliwal on 07-06-2017.
 */
class ActivityStack {
       companion object activityStack{
        private var instance: ActivityStack? = null
        private val activities =  ArrayList<AppCompatActivity>()
           fun  getInstance(context: Context): ActivityStack {
               if (instance == null)
                   instance = ActivityStack()
               return instance!!
           }

       public fun addActivity(activity: AppCompatActivity) {
            if (!activities.contains(activity)) {
                activities.add(activity)
            }
        }

        fun removeActivity(activity: AppCompatActivity?) {
            if (activity != null) {
                activities.remove(activity)
            }
        }

           internal fun getActivities(): ArrayList<AppCompatActivity> {
               return activities
           }

        /**
         * Cleared available object in array.
         */
        fun cleareAll() {
            val activities2 = getActivities()
            for (activity in activities2) {
                activity.finish()
            }
            activities.clear()

        }
    }
}