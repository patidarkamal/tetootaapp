package com.tetoota.listener

/**
 * Created by charchit.kasliwal on 12-06-2017.
 */
interface IAdapterClickListener {
    fun cellItemClick(mString : String, cellRow : Any) : Unit
}