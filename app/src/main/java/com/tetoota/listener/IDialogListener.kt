package com.tetoota.listener

/**
 * Created by charchit.kasliwal on 31-05-2017.
 */
interface IDialogListener {
    /**
     * @param param the param value
     * *
     * @param message the message on yess press
     */
    fun onYesPress(param: String, message: String)
}