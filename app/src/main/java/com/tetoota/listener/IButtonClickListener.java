package com.tetoota.listener;

/**
 * Created by abhinav.maurya on 28-11-2016.
 */

public interface IButtonClickListener {

    public void onClickRemove(int position);
}
