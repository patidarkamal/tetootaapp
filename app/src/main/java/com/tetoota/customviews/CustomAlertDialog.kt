package com.tetoota.customviews

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.WindowManager
import com.tetoota.R
import com.tetoota.listener.IDialogListener
import kotlinx.android.synthetic.main.custom_alert_layout.*
import org.jetbrains.anko.onClick

/**
 * Created by charchit.kasliwal on 07-06-2017.
 */
class CustomAlertDialog : Dialog{
    private val iDialogListener: IDialogListener
    constructor(context: Context?, dialogID : Int,iDialogListener: IDialogListener, message : String) : super(context, R.style.newDialog) {
        window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        window!!.attributes.windowAnimations = R.style.DialogAnimation
        setContentView(R.layout.custom_alert_layout)
        this.iDialogListener = iDialogListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        btn_ok.onClick {
            dismiss()
            println("Btn ok")
        }
    }


}