package com.tetoota.google_translation

import com.google.gson.annotations.SerializedName

data class GoogleTranslateResponse(

	@field:SerializedName("data")
	val data: Data? = null
)