package com.tetoota.google_translation

import com.tetoota.google_translation.GoogleLanguagesResponce.LanguagesItem

/**
 * Created by jitendra.nandiya on 03-01-2018.
 */
class GoogleTranslatePresenter : GoogleTranslateContract.Presenter, GoogleTranslateContract.GoogleTranslateApiResult {

    var GoogleTranslateView: GoogleTranslateContract.View

    override fun onGoogleDetectLanguageApiSuccess(mString: String, mLanguageCode: String) {
        GoogleTranslateView.onGoogleDetectLanguageSuccess(mString, mLanguageCode)
    }

    override fun onGoogleDetectLanguageApiFailure(mString: String, isServerError: Boolean) {
        GoogleTranslateView.onGoogleDetectLanguageFailureResponse(mString, isServerError)
    }

    override fun getGoogleLanguages(key: String, target: String) {
        mFavoriteInteractor.getGoogleLanguages(key, target)
    }

    override fun onGoogleTranslateApiSuccess(mString: String, mReferralCode: String) {
        GoogleTranslateView.onGoogleTranslateSuccess(mString, mReferralCode)
    }

    override fun onGoogleLanguagesApiSuccess(mString: String, mLanguagesList: List<LanguagesItem?>) {
        GoogleTranslateView.onGoogleLanguagesSuccess(mString, mLanguagesList)
    }

    override fun onGoogleTranslateApiFailure(mString: String, isServerError: Boolean) {
        GoogleTranslateView.onGoogleTranslateFailureResponse(mString, isServerError)
    }

    val mFavoriteInteractor: GoogleTranslateInteractor by lazy {
        GoogleTranslateInteractor(this)
    }

    constructor(mFavoriteView: GoogleTranslateContract.View) {
        this.GoogleTranslateView = mFavoriteView
    }

    override fun getGoogleTranslateText(key: String, q: String, source: String, target: String, format: String) {
        mFavoriteInteractor.getGoogleTranslate(key, q, source, target, format)
    }

    override fun getGoogleDetectLanguage(key: String, q: String) {
        mFavoriteInteractor.getGoogleDetectLanguage(key, q)
    }



    /*override fun onRedeemCouponsApiSuccess(mString: String, mReferralCode: String) {
        mFavoriteView.onRedeemCouponsSuccess(mString, mReferralCode)
    }*/

    /*override fun onRedeemCouponsApiFailure(mString: String, isServerError: Boolean) {
        mFavoriteView.onRedeemCouponsFailureResponse(mString, isServerError)
    }*/
}