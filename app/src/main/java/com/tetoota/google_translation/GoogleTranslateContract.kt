package com.tetoota.google_translation

import com.tetoota.google_translation.GoogleLanguagesResponce.LanguagesItem

/**
 * Created by jitendra.nandiya on 03-01-2018.
 */
class GoogleTranslateContract {
    interface View {
        fun onGoogleLanguagesSuccess(mString: String, mLanguagesList: List<LanguagesItem?>): Unit
        fun onGoogleDetectLanguageSuccess(mString : String, mLanguageCode: String):Unit
        fun onGoogleTranslateSuccess(mString: String, mFavoriteList: String): Unit
        fun onGoogleTranslateFailureResponse(mString: String, isServerError: Boolean): Unit
        fun onGoogleDetectLanguageFailureResponse(mString: String, isServerError: Boolean): Unit
    }

    interface Presenter {
        fun getGoogleTranslateText(key: String,
                                   q: String,
                                   source: String,
                                   target: String,
                                   format: String)

        fun getGoogleLanguages(key: String, target: String)
        fun getGoogleDetectLanguage(key: String, q:String)
    }

    interface GoogleTranslateApiResult {
        fun onGoogleLanguagesApiSuccess(mString: String, mLanguagesList: List<LanguagesItem?>): Unit
        fun onGoogleTranslateApiSuccess(mString: String, mReferralCode: String): Unit
        fun onGoogleDetectLanguageApiSuccess(mString: String, mLanguageCode:String):Unit
        fun onGoogleDetectLanguageApiFailure(mString: String, isServerError: Boolean): Unit
        fun onGoogleTranslateApiFailure(mString: String, isServerError: Boolean): Unit
    }
}