package com.tetoota.google_translation.GoogleLanguagesResponce

import com.google.gson.annotations.SerializedName
data class LanguagesItem(
	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("language")
	val language: String? = null
)