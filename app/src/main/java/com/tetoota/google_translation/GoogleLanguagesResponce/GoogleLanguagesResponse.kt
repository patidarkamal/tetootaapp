package com.tetoota.google_translation.GoogleLanguagesResponce

import com.google.gson.annotations.SerializedName

data class GoogleLanguagesResponse(

	@field:SerializedName("data")
	val data: Data? = null
)