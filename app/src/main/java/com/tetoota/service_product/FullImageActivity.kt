package com.tetoota.service_product

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.R
import kotlinx.android.synthetic.main.activity_full_image.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick

class FullImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_image)

        initToolbar()
        if (intent != null) {
            var profile_image1 = intent?.getStringExtra("profile_image")
            Picasso.get().load(profile_image1).into(profile_image)
        }


    }

    private fun initToolbar() : Unit {
        iv_close.visibility = View.VISIBLE
        toolbar_title.text = "Tetoota"
        iv_close.onClick {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@FullImageActivity)
        finish()
    }
    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, FullImageActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

}
