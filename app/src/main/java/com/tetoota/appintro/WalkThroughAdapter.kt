package com.tetoota.appintro

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.tetoota.R
import com.tetoota.utility.Utils

/**
 * Created by charchit.kasliwal on 09-06-2017.
 */
class WalkThroughAdapter(val mContext: Context,
                         var mWalkThroughList: List<WalkThroughDataResponse> = ArrayList<WalkThroughDataResponse>())
    : PagerAdapter() {
    /**
     *
     */
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as RelativeLayout
    }

    fun setElements(mWalkThroughList: ArrayList<WalkThroughDataResponse>) {
        this.mWalkThroughList = mWalkThroughList
    }

    /**
     *
     */
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container?.removeView(`object` as RelativeLayout)
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    /**
     *
     */
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(mContext).inflate(R.layout.walkthrough_item, container, false)

        val imageView = itemView.findViewById<ImageView>(R.id.img_pager_item)
        val tv_text_Heading = itemView.findViewById<TextView>(R.id.tv_text_heading)
        val tv_text_Sub_Heading = itemView.findViewById<TextView>(R.id.tv_text_subheading)
        val rl_walkthroughtext = itemView.findViewById<RelativeLayout>(R.id.rl_walkthroughtext)
        // imageView.setImageResource(mResources[position].slidesImage)
        var mWalkThroughDataResp = mWalkThroughList[position]
        tv_text_Heading.text = mWalkThroughList[position].slide_heading
        tv_text_Sub_Heading.text = Utils.Utils.fromHtml(mWalkThroughDataResp.slide_content!!)
        rl_walkthroughtext.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_start_conversation_btn))
        tv_text_Heading.setTextColor(ContextCompat.getColor(mContext, R.color.color_white))
        tv_text_Sub_Heading.setTextColor(ContextCompat.getColor(mContext, R.color.color_white))

//      rl_walkthroughtext.visibility = View.GONE

        if (mWalkThroughDataResp.slides_image2!!.isEmpty()) {
            imageView.setImageResource(R.drawable.queuelist_place_holder);
        } else{

            Picasso.get().load(mWalkThroughDataResp.slides_image2!!).into(imageView)
        }


/*
        Glide.with(itemView.context)
                .load(Utils.getUrl(itemView.context, mWalkThroughDataResp.slides_image2!!))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .dontAnimate()
                .override(Utils.getScreenWidth(mContext), Utils.getScreenHeight(mContext))
                .into(imageView)
*/

        container.addView(itemView)
        return itemView
    }

    override fun getCount(): Int {
        return mWalkThroughList.size
    }
}