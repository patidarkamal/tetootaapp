package com.tetoota.appintro

data class WalkThroughDataResponse(
	val slides_image1: String? = null,
	val slides_image2: String? = null,
	val slides_image3: String? = null,
	val slide_text_color: String? = null,
	val slide_heading: String? = null,
	val slide_background_color: String? = null,
	val slide_content: String? = null,
	val language: String? = null
)
