package com.tetoota.addrequest

import android.os.Parcel
import android.os.Parcelable

data class TradingDataResponse(
        val description: String? = null,
        val language: String? = null,
        val id: Int? = null) : Parcelable {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<TradingDataResponse> = object : Parcelable.Creator<TradingDataResponse> {
            override fun createFromParcel(source: Parcel): TradingDataResponse = TradingDataResponse(source)
            override fun newArray(size: Int): Array<TradingDataResponse?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(description)
        dest.writeString(language)
        dest.writeValue(id)
    }
}
