package com.tetoota.addrequest

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.tetoota.R
import com.tetoota.categories.CategoriesDataResponse
import com.tetoota.selectlanguage.LanguageDataResponse
import com.tetoota.selectlanguage.SelectLanguageAdapter
import com.tetoota.subcategories.DataItem

/**
 * Created by charchit.kasliwal on 11-07-2017.
 */
class BottomSheetDialog : BottomSheetDialogFragment(), AddRequestAdapter.ItemListener,
        AddRequestCategoryData.ItemListener, SelectLanguageAdapter.IAdapterClickListener
        , AddRequestSubCategoryData.ItemListener, AddNormalRequestAdapter.ItemListener{

    private var mBehavior: BottomSheetBehavior<*>? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        val view = View.inflate(context, R.layout.bottom_sheet_layout, null)
        view.findViewById<View>(R.id.fakeShadow).visibility = View.GONE
        val recyclerView = view.findViewById<RecyclerView>(R.id.rv_bottom_sheet)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(context)
        if (type == "trading") {
            val itemAdapter = AddRequestAdapter(createItems(), this)
            recyclerView.adapter = itemAdapter
        } else if(type == "language"){
            val itemAdapter = SelectLanguageAdapter(createLanguageItems(),iAdapterClickListener = this)
            recyclerView.adapter = itemAdapter
        }else if(type == "subcategory"){
            val itemAdapter = AddRequestSubCategoryData(subCreateCategoriesItems(), this)
            recyclerView.adapter = itemAdapter
        }else if(type == "quota" || type == "points"){
            val itemAdapter = AddNormalRequestAdapter(createNormalItems(), this)
            recyclerView.adapter = itemAdapter
        }else {
            val itemAdapter = AddRequestCategoryData(createCategoriesItems(), this)
            recyclerView.adapter = itemAdapter
        }
        dialog.setContentView(view)
        mBehavior = BottomSheetBehavior.from(view.parent as View)
        return dialog
    }

    private fun createCategoriesItems(): ArrayList<CategoriesDataResponse> {
        var items = ArrayList<CategoriesDataResponse>()
        items = param2!!;
        return items
    }

    private fun subCreateCategoriesItems(): ArrayList<DataItem> {
        var items = ArrayList<DataItem>()
        items = param2Subcategory!!;
        return items
    }

    private fun createNormalItems(): ArrayList<String> {
        var items = ArrayList<String>()
        items = param!!
        return items
    }

    override fun onStart() {
        super.onStart()
        mBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
    }

    fun createItems(): ArrayList<TradingDataResponse> {
        var items = ArrayList<TradingDataResponse>()
        items = param1!!
        return items
    }

    fun createLanguageItems(): ArrayList<LanguageDataResponse> {
        var items = ArrayList<LanguageDataResponse>()
        items = param3!!
        return items
    }

    override fun onItemClick(item: TradingDataResponse) {
        if (iBottomSheet != null) {
            iBottomSheet!!.dataClick(item, "trading")
        }
        mBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        // iBottomSheet?.dataClick(item)
    }

    override fun cellItemClick(mString: String, cellRow: Any) {
        if (iBottomSheet != null) {
            iBottomSheet!!.dataClick(cellRow as LanguageDataResponse, "language")
        }
        mBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
    }

    override fun onItemClickListener(item: CategoriesDataResponse) {
        if (iBottomSheet != null) {
            iBottomSheet!!.dataClick(item, "category")
        }
        mBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
    }

    override fun onItemClickListener(item: DataItem) {
        if (iBottomSheet != null) {
            iBottomSheet!!.dataClick(item, "subcategory")
        }
        mBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
    }

    override fun onItemClick(item: String) {
        if (iBottomSheet != null) {
            iBottomSheet!!.dataClick(item, type)
        }
        mBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
    }

    override fun onDetach() {
        super.onDetach()
        iBottomSheet = null
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

    }

    interface IBottomSheetListener {
        fun dataClick(mTredingDataResponse: Any, type: String)
    }


    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        private val ARG_PARAM2SUBCATEGORY = "param2Subcategory"
        private val ARG_PARAM3 = "param3"
        private var type = ""
        private var param1: ArrayList<TradingDataResponse>? = null

        var iBottomSheet: IBottomSheetListener? = null
        private var param2: ArrayList<CategoriesDataResponse>? = null
        private var param2Subcategory: ArrayList<DataItem>? = null
        private var param3: ArrayList<LanguageDataResponse>? = null
        private var param: ArrayList<String>? = null

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance1(param1: ArrayList<TradingDataResponse>, iBottomSheetListener: IBottomSheetListener,
                         type: String): com.tetoota.addrequest.BottomSheetDialog {
            val fragment = BottomSheetDialog()
            this.type = type
            this.iBottomSheet = iBottomSheetListener
            val args = Bundle()
            args.putParcelableArrayList(ARG_PARAM1, param1)
            this.param1 = param1
            fragment.arguments = args
            return fragment
        }


        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance2(param3: ArrayList<LanguageDataResponse>, iBottomSheetListener: IBottomSheetListener,
                         type: String): com.tetoota.addrequest.BottomSheetDialog {
            val fragment = BottomSheetDialog()
            this.type = type
            this.iBottomSheet = iBottomSheetListener
            val args = Bundle()
            args.putParcelableArrayList(ARG_PARAM3, param3)
            this.param3 = param3
            fragment.arguments = args
            return fragment
        }

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param2: ArrayList<CategoriesDataResponse>, iBottomSheetListener: IBottomSheetListener
                        , type: String): com.tetoota.addrequest.BottomSheetDialog {
            val fragment = BottomSheetDialog()
            this.type = type
            this.iBottomSheet = iBottomSheetListener
            val args = Bundle()
            args.putParcelableArrayList(ARG_PARAM2, param2)
            this.param2 = param2
            fragment.arguments = args
            return fragment
        }

        // TODO: Rename and change types and number of parameters
        fun newSubCategoryInstance(param2: ArrayList<DataItem>, iBottomSheetListener: IBottomSheetListener
                        , type: String): com.tetoota.addrequest.BottomSheetDialog {
            val fragment = BottomSheetDialog()
            this.type = type
            this.iBottomSheet = iBottomSheetListener
            val args = Bundle()
            args.putParcelableArrayList(ARG_PARAM2SUBCATEGORY, param2)
            this.param2Subcategory = param2
            fragment.arguments = args
            return fragment
        }

        fun newInstance(param: ArrayList<String>, iBottomSheetListener: IBottomSheetListener
                        , type: String, noUse: String): com.tetoota.addrequest.BottomSheetDialog {
            val fragment = BottomSheetDialog()
            this.type = type
            this.iBottomSheet = iBottomSheetListener
            val args = Bundle()
            args.putStringArrayList(ARG_PARAM2, param)
            this.param = param
            fragment.arguments = args
            return fragment
        }
    }
}