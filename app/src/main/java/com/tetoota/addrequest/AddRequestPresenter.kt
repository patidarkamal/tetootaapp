package com.tetoota.addrequest
import android.app.Activity
import com.tetoota.fragment.home.model.DataItem
import okhttp3.RequestBody

/**
 * Created by charchit.kasliwal on 11-07-2017.
 */
class AddRequestPresenter  : AddRequestContract.Presenter, AddRequestContract.addRequestApiListener {

    var mAddRequest : AddRequestContract.View
    private val mAddReqInteractor : AddRequestInteractor by lazy {
        AddRequestInteractor(this)
    }

    constructor(mAddRequest: AddRequestContract.View) {
        this.mAddRequest = mAddRequest
    }
    override fun getTredingPrefrenceData(mActivity: Activity) {
        mAddReqInteractor.getTredingDataFromServer(mActivity)
    }

    override fun editDataListener(message: String) {
        super.editDataListener(message)
        mAddRequest.onEditDataListener(message)
    }

    override fun addService(mActivity: Activity, map: HashMap<String, RequestBody>, mImageList: List<String>) {
        mAddReqInteractor.addServiceToServer(mActivity,map,mImageList)
    }
    override fun deleteService(mActivity: Activity, userId: String, postId: String) {
        mAddReqInteractor.deleteServiceFromServer(mActivity,userId,postId)
    }

    override fun editService(mActivity: Activity, map: HashMap<String, RequestBody>, mImageList: List<String>) {
        super.editService(mActivity, map, mImageList)
        mAddReqInteractor.editServiceToServer(mActivity,map,mImageList)
    }

    override fun getCategoryData(mActivity: Activity) {
    }

    override fun onSuccess(mDataList: ArrayList<Any>, message: String) {
        mAddRequest.onApiSuccess(mDataList, message)
    }

    override fun onDeletePostFailure(message: String) {
        mAddRequest.onApiFailure(message)
    }

    override fun onDeletePostSuccess(message: String) {
        mAddRequest.onDeletePostApiSuccess(message)
    }

    override fun onCategoryListener(message: String, responseType : String) {
        mAddRequest.onCategoryApiSuccess(message,responseType)
    }

}