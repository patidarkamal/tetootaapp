package com.tetoota.addrequest

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tetoota.R
import kotlinx.android.synthetic.main.selectlanguage_row.view.*
import org.jetbrains.anko.onClick

/**
 * Created by abhinav.maurya on 02-02-2018.
 */
class AddNormalRequestAdapter (var mSelectLanguageList : ArrayList<String> = ArrayList<String>(),
                               val iAdapterClickListener : ItemListener)
    : RecyclerView.Adapter<AddNormalRequestAdapter.ViewHolder>() {

    /********************************************************/
    override fun onBindViewHolder(viewHolder: ViewHolder, p1: Int) {
        viewHolder.bindHomeData(viewHolder as ViewHolder?, p1, mSelectLanguageList[p1],iAdapterClickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.selectlanguage_row, parent, false))
    }
/********************************************************************/
 /*   override fun onCreateViewHolder(parent: ViewGroup?, p1: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        return ViewHolder(layoutInflater.inflate(R.layout.selectlanguage_row, parent, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder?, p1: Int) {
        viewHolder?.bindHomeData(viewHolder as ViewHolder?, p1, mSelectLanguageList[p1],iAdapterClickListener)
    }*/

    fun setElements(mSelectLanguageList: ArrayList<String>) {
        this.mSelectLanguageList = mSelectLanguageList
    }
    private fun getLastPosition() = if (mSelectLanguageList.lastIndex == -1) 0 else mSelectLanguageList.lastIndex

    override fun getItemCount(): Int {
        return mSelectLanguageList.size
    }

    interface ItemListener {
        fun onItemClick(item: String)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_language_text = view.tv_language_text!!
        val rl_maincontent = view.rl_maincontent!!
        fun bindHomeData(viewHolder: ViewHolder?, p1: Int, mTradingRespon: String, iAdapterClickListener: ItemListener) {
            tv_language_text.text = mTradingRespon
            rl_maincontent.onClick {
                iAdapterClickListener.onItemClick(mTradingRespon)
            }
        }
    }
}