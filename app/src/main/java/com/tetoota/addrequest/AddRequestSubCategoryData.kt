package com.tetoota.addrequest

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tetoota.R
import com.tetoota.subcategories.DataItem
import kotlinx.android.synthetic.main.selectlanguage_row.view.*
import org.jetbrains.anko.onClick
import java.util.*

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class AddRequestSubCategoryData (var mSelectLanguageList : ArrayList<DataItem> = ArrayList<DataItem>(),
                                 val iAdapterClickListener : ItemListener)
    : RecyclerView.Adapter<AddRequestSubCategoryData.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.selectlanguage_row, parent, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, p1: Int) {
        viewHolder.bindHomeData(viewHolder, p1, mSelectLanguageList[p1],iAdapterClickListener)
    }

    fun setElements(mSelectLanguageList: ArrayList<DataItem>) {
        this.mSelectLanguageList = mSelectLanguageList
    }
    private fun getLastPosition() = if (mSelectLanguageList.lastIndex == -1) 0 else mSelectLanguageList.lastIndex

    override fun getItemCount(): Int {
        return mSelectLanguageList.size
    }

    interface ItemListener {
        fun onItemClickListener(item: DataItem)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_language_text = view.tv_language_text!!
        val rl_maincontent = view.rl_maincontent!!
        fun bindHomeData(viewHolder: ViewHolder?, p1: Int, mTradingRespon: DataItem, iAdapterClickListener: ItemListener) {
            tv_language_text.text = mTradingRespon.sub_category_name
            rl_maincontent.onClick {
                iAdapterClickListener.onItemClickListener(mTradingRespon)
            }
        }
    }
}