package com.tetoota.database
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.util.Log
import com.tetoota.selectlanguage.SelectedLanguageDataResponse
import java.util.*
/**
 * Created by vaibhav.malviya on 13-07-2017.
 */
class DBAdapter(ctx: Context) {
    lateinit var context: Context
    lateinit var mDbObject: SQLiteDatabase
    var mDBHelper: DataBaseHelper = DataBaseHelper(ctx)

    // Open the database connection.
    fun open(): DBAdapter {
        mDbObject = mDBHelper.writableDatabase
        return this
    }


    // Close the database connection.
    fun close() {
        mDBHelper.close()
    }


    fun insertLanguage(LanguageList: MutableList<SelectedLanguageDataResponse>): Boolean {
        if (LanguageList == null || LanguageList.size == 0)
            return false
        var sql: String = "INSERT OR REPLACE INTO " +
                "${IDbKey.LanguageEntry.TABLE_NAME} ( " +
                "${IDbKey.LanguageEntry.COLUMN_NAME_langCode}" + "  ," +
                "${IDbKey.LanguageEntry.COLUMN_NAME_labelkey}  ," +
                "${IDbKey.LanguageEntry.COLUMN_NAME_labelVal}  ," +
                "${IDbKey.LanguageEntry.COLUMN_NAME_updatedDate}" +
                ") VALUES(?1, ?2, ?3, ?4)"
        /* SQLiteDatabase db = getWritableDb();*/
        val stmt = mDbObject.compileStatement(sql)
        mDbObject.beginTransaction()
        try {
            try {
                for (item in LanguageList) {
                    stmt.bindString(1, item.langCode)
                    stmt.bindString(2, item.labelkey)
                    stmt.bindString(3, item.labelVal)
                    stmt.bindString(4, item.updatedDate)

                    stmt.execute()
                }
                mDbObject.setTransactionSuccessful()
                return true
            } catch (e: SQLiteException) {
                // AppLog.e(AppLog.T.COMMENTS, e);
                Log.d("Insert SQLiteException", e.toString());
                return false
            }
        } finally {
            mDbObject.endTransaction()
            stmt?.close()
        }
    }

    fun getLanguageByCode(languageCode: String): HashMap<String, String> {
        val languageMap: HashMap<String, String> = HashMap()
        var languageList: MutableList<SelectedLanguageDataResponse> = mutableListOf()
        var selectQuery: String = "SELECT  * FROM  ${IDbKey.LanguageEntry.TABLE_NAME} where ${IDbKey.LanguageEntry.COLUMN_NAME_langCode} = '$languageCode' "
        //  var selectQuery: String = "SELECT  * FROM  ${IDbKey.OfferEntry.TABLE_NAME} "
        val cursor = mDbObject.rawQuery(selectQuery, null)
        var languageModel: SelectedLanguageDataResponse
        val columnIndex_02 = cursor.getColumnIndex(IDbKey.LanguageEntry.COLUMN_NAME_labelkey)
        val columnIndex_03 = cursor.getColumnIndex(IDbKey.LanguageEntry.COLUMN_NAME_labelVal)

        if (cursor != null)
            if (cursor.moveToFirst()) {
                do {
                    languageMap.put(cursor.getString(columnIndex_02), cursor.getString(columnIndex_03))
                    /* languageModel = LanguageModel(cursor.getString(columnIndex_01), cursor.getString(columnIndex_02)
                             , cursor.getString(columnIndex_03), cursor.getString(columnIndex_04))

                     languageList.add(languageModel)*/
                } while (cursor.moveToNext())
            }
        Log.d("DB Adapter", "" + languageList)
        // return contact list
        return languageMap
    }

    fun getLanguageValueByKey(languageCode: String, languageKey: String): String {
        var languageList: MutableList<SelectedLanguageDataResponse> = mutableListOf()
        var selectQuery: String = "SELECT  ${IDbKey.LanguageEntry.COLUMN_NAME_labelVal} FROM  ${IDbKey.LanguageEntry.TABLE_NAME} where ${IDbKey.LanguageEntry.COLUMN_NAME_langCode} = '$languageCode' " +
                "AND ${IDbKey.LanguageEntry.COLUMN_NAME_labelkey} = '$languageKey' "
        //  var selectQuery: String = "SELECT  * FROM  ${IDbKey.OfferEntry.TABLE_NAME} "
        val cursor = mDbObject.rawQuery(selectQuery, null)
        val columnIndex_01 = cursor.getColumnIndex(IDbKey.LanguageEntry.COLUMN_NAME_labelVal)
        if (cursor != null)
            if (cursor.moveToFirst()) {

                return cursor.getString(columnIndex_01)
            }
        Log.d("DB Adapter", "" + languageList)
        // return contact list
        return null!!
    }
}



