package com.tetoota.database;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
public class DataBaseHelper extends SQLiteOpenHelper {
    private SQLiteDatabase mDataBase = null;
    private Context mContext = null;
    private static final String DATABASE_NAME = "Tetoota.sqlite";
    public static String DATABASE_PATH = "";
    public static final int DATABASE_VERSION = 1;

    private static final String CREATE_TABLE_LANGUAGEMASTER = "CREATE TABLE IF NOT EXISTS "
            + IDbKey.LanguageEntry.TABLE_NAME + "("
            + IDbKey.LanguageEntry.COLUMN_NAME_id
            + " INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL ,"
            + IDbKey.LanguageEntry.COLUMN_NAME_langCode
            + " text ,"
            + IDbKey.LanguageEntry.COLUMN_NAME_labelkey
            + " text ,"
            + IDbKey.LanguageEntry.COLUMN_NAME_labelVal
            + " text ,"
            + IDbKey.LanguageEntry.COLUMN_NAME_updatedDate
            + " text );";



    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
        DATABASE_PATH = mContext.getDatabasePath(DATABASE_NAME).toString();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_LANGUAGEMASTER);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DataBaseHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion
                + ", which will destroy all old pushNotificationDataResponse");
        db.execSQL("DROP TABLE IF EXISTS " + IDbKey.LanguageEntry.TABLE_NAME);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    //add your public methods for insert, get, delete and update pushNotificationDataResponse in database.
}
