package com.tetoota.checkUser;


import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


/**
 * Created by kamal on 7/19/2016.
 */
public interface RetrofitService {

    @FormUrlEncoded
    @POST("authentication/checkUser")
    Call<ResponseBody> checkUser(@FieldMap HashMap<String, String> hm);
}



