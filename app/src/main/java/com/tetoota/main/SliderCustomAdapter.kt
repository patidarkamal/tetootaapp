package com.tetoota.main

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.tetoota.R
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.nav_bar_item_row.view.*
import org.jetbrains.anko.imageResource

/**
 * Created by charchit.kasliwal on 31-05-2017.
 */
class SliderCustomAdapter : BaseAdapter {
    var mContext: Context? = null
    var mSidemenuList = arrayListOf<SideMenu>()

    constructor(mContext: Context?, mArrayList: ArrayList<SideMenu>) : super() {
        this.mContext = mContext
        this.mSidemenuList = mArrayList
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val mView: View
        if (convertView == null) {
            val inflater = mContext?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            mView = inflater.inflate(R.layout.nav_bar_item_row, null)
        } else {
            mView = convertView
        }
        mView.menu_click.findViewById<RelativeLayout>(R.id.menu_click)
        mView.parent_layout.findViewById<LinearLayout>(R.id.parent_layout)
        mView.tv_unread_count.findViewById<TextView>(R.id.tv_unread_count)
        mView.top_shadow_line.findViewById<View>(R.id.top_shadow_line)
        mView.bottom_divider_line.findViewById<View>(R.id.bottom_divider_line)
        if(position == 2){
            mView.tv_unread_count.visibility = View.VISIBLE
            mView.tv_unread_count.text = Utils.Utils.loadInt(Constant.USER_PROPOSAL_COUNT).toString()

        }else{
            mView.tv_unread_count.visibility = View.GONE
        }
        val sideMenu: SideMenu = mSidemenuList[position]
        mView.title.text = sideMenu.name
        mView.menu_icon.imageResource = sideMenu.drawable
        if (sideMenu.isSelected) {
            mView.menu_click.setBackgroundColor(ContextCompat.getColor(this.mContext!!, R.color.colorPrimaryDark))
            mView.menu_click.background.alpha = 180
            mView.parent_layout.setBackgroundColor(ContextCompat.getColor(this.mContext!!, R.color.color_white))
        } else {
            mView.menu_click.setBackgroundColor(ContextCompat.getColor(this.mContext!!, android.R.color.transparent))
            mView.menu_click.background.alpha = 180
            mView.parent_layout.setBackgroundColor(ContextCompat.getColor(this.mContext!!, android.R.color.transparent))
        }
        return mView
    }

    override fun getItem(position: Int): Any {
        return mSidemenuList[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return mSidemenuList.size
    }
}