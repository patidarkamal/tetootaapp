package com.tetoota.login
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.AppCompatButton
import android.view.View
import com.tetoota.BaseActivity
import com.tetoota.R
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : BaseActivity(){
    private val FRAMEWORK_REQUEST_CODE = 1
    /**
     * On Create
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btn_next.findViewById<AppCompatButton>(R.id.btn_next)
        btn_next.setOnClickListener {
            if(!validate()){
                progress.visibility = View.VISIBLE
                //mLoginPresenter.checkUserLogin(this,edt_mobilenumber.text.toString().trim())
            }
            else{
            showSnackBar("Invalid Mobile Number") }

        }
    }
    override fun onDestroy() {
        super.onDestroy()
        progress.visibility = View.GONE
    }

    /**
     * Method to Check Mobile Number Validation
     */
    fun validate() : Boolean{
        println("Check Mobile Number")
        if(edt_mobilenumber.length() >= 9)

        return false
        return true
    }

    companion object{
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, LoginActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

}
