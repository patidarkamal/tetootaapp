package com.tetoota.fragment.settings
import com.tetoota.network.errorModel.Meta
data class PushSettingsResponse(
		val data: PushNotificationDataResponse? = null,
		val meta: Meta? = null
)
