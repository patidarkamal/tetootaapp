package com.tetoota.pushnotificationsetting
import android.app.Activity
/**
 * Created by charchit.kasliwal on 27-10-2017.
 */
class PushSettingsPresenter : PushSettingsContract.Presenter, PushSettingsContract.pushNotificationListener{


    var mPushContract : PushSettingsContract.View

    private val mPushInteractor : PushSettingsInteractor by lazy {
        PushSettingsInteractor(this)
    }

    constructor(mLanguageContractView: PushSettingsContract.View){
        this.mPushContract = mLanguageContractView
    }

    override fun onPushNotificationSuccess(response: String, message: String) {
        mPushContract.pushNotificationSuccess(response,message)
    }

    override fun onPushNotificationFailure(response: String, message: String) {
        mPushContract.pushNotificationFailure(response,message)
    }

    override fun setUserVisibiltySettings(mActivity: Activity, mUserId: String, mIsUserVisible: String) {
        mPushInteractor.userVisibility(mActivity,mUserId,mIsUserVisible)
    }


    override fun setPushNotificationSetting(mActivity: Activity, all_push: String,proposal_push: String,
                                            chat_push: String) {
        mPushInteractor.setPushSettings(mActivity,all_push,proposal_push,chat_push)

    }
}