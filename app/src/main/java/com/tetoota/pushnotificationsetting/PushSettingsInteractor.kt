package com.tetoota.pushnotificationsetting

import android.app.Activity
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.fragment.settings.PushSettingsResponse
import com.tetoota.fragment.settings.UserVisibilityResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by charchit.kasliwal on 27-10-2017.
 */
class PushSettingsInteractor {
    private var mPushListener : PushSettingsContract.pushNotificationListener

    constructor(mPushListener: PushSettingsContract.pushNotificationListener) {
        this.mPushListener = mPushListener
    }
    fun setPushSettings(mActivity : Activity, mAllProposal : String, mProposal : String, mChat : String){
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData  = Gson().fromJson(json, LoginDataResponse::class.java)
        var call: Call<PushSettingsResponse> = TetootaApplication.getHeader()
                .setNotificationSetting(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity)
                        , Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity), personData.user_id!!
                        ,mAllProposal,mProposal,mChat)
        call.enqueue(object : Callback<PushSettingsResponse> {
            override fun onResponse(call: Call<PushSettingsResponse>?,
                                    response: Response<PushSettingsResponse>?) {
                if(response?.code() == 200){
                    Utils.savePreferences(Constant.ALL_PUSH_NOTIFICATION,
                            response.body()?.data?.all_notification.toString(),mActivity)
                    Utils.savePreferences(Constant.PROPOSAL_PUSH_NOTIFICATION,
                            response.body()?.data?.proposal_notification.toString(),mActivity)
                    Utils.savePreferences(Constant.CHAT_PUSH_NOTIFICATION,
                            response.body()?.data?.chat_notification.toString(),mActivity)
                    mPushListener.onPushNotificationSuccess("success",response.message())

                }else{
                    mPushListener.onPushNotificationFailure("success", response?.message()!!)
                }
            }
            override fun onFailure(call: Call<PushSettingsResponse>?, t: Throwable?) {
                mPushListener.onPushNotificationFailure("success", t?.message.toString())
              //  mSelectApiListener.onLanguageApiFailure(t?.message.toString())
            }
        })

    }

    fun userVisibility(mActivity: Activity,mUserId : String,isVisible : String){
        var call: Call<UserVisibilityResponse> = TetootaApplication.getHeader()
                .setUserVisibiltySettings(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity)
                        , Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity), mUserId,isVisible)
        call.enqueue(object : Callback<UserVisibilityResponse> {
            override fun onResponse(call: Call<UserVisibilityResponse>?,
                                    response: Response<UserVisibilityResponse>?) {
                if(response?.code() == 200){
                    Utils.savePreferences(Constant.IS_VISIBLE,
                            response.body()?.data?.is_visible.toString(),mActivity)
                    mPushListener.onPushNotificationSuccess("success",response.message())
                }else{
                    mPushListener.onPushNotificationFailure("success", response?.message()!!)
                }
            }
            override fun onFailure(call: Call<UserVisibilityResponse>?, t: Throwable?) {
                mPushListener.onPushNotificationFailure("success", t?.message.toString())
                //  mSelectApiListener.onLanguageApiFailure(t?.message.toString())
            }
        })
    }

}