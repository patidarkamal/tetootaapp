package com.tetoota.proposal.CheckProposalsDataResponce

import com.google.gson.annotations.SerializedName

data class CheckProposalsDataResponse(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("meta")
	val meta: Meta? = null
)