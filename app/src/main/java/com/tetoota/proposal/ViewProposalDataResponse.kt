package com.tetoota.proposal

data class ViewProposalDataResponse(
	val proposal_status: String? = null,
	val proposal_id: String? = null,
	val latitude: String? = null,
	val friendliness: String? = null,
	val review_message: String? = null,
	val description: String? = null,
	val availability: String? = null,
	val trading_status: String? = null,
	val title: String? = null,
	val proposal_to_user_profile_img: String? = null,
	val proposal_from_user_profile_img: String? = null,
	val average_badges: String? = null,
	val proposal_to_user_lastName: String? = null,
	val quality_services: String? = null,
	val exchange_post_record : List<Any>? = null,
	val post_type: String? = null,
	val longitude: String? = null,
	val post_image: String? = null,
	val prop_tetoota_points: String? = null,
	val proposal_from_user_lastName: String? = null,
	val proposal_to_user_firstName: String? = null,
	val trading_preference: Int? = null,
	val creation_date: String? = null,
	val hours_left: String? = null,
	val set_quote: String? = null,
	val proposal_create_datetime: String? = null,
	val qualification: String? = null,
	val post_id: Int? = null,
	val proposal_from_user_firstName: String? = null,
	val user_id: String? = null,
	val share_service_platform: String? = null,
	val response_time: String? = null,
	val set_tetoota_points: String? = null,
	val categories_ids: String? = null,
	val virtual_service: String? = null,
	val proposal_time: String? = null
)
