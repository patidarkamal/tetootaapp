package com.tetoota.proposal
import com.tetoota.network.errorModel.Meta
data class ProposalActionResponse(
	val data: List<Any?>? = null,
	val meta: Meta? = null
)
