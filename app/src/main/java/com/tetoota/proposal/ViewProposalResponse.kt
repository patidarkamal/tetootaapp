package com.tetoota.proposal
import com.tetoota.network.errorModel.Meta
data class ViewProposalResponse(
        val data: List<ViewProposalDataResponse?>? = null,
        val meta: Meta? = null
)
