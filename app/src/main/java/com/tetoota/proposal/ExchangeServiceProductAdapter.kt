package com.tetoota.proposal

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tetoota.R
import com.tetoota.fragment.dashboard.ServicesDataResponse
import kotlinx.android.synthetic.main.exchange_service_product_list_row.view.*
import org.jetbrains.anko.onClick

/**
 * Created by jitendra.nandiya on 10-08-2017.
 */
class ExchangeServiceProductAdapter(var mProductList: MutableList<Any?> = ArrayList<Any?>(),
                                    var iAdapterClickListener: IAdapterClick)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.exchange_service_product_list_row, parent, false)
        return ViewHolder(view)
        // return null
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, p1: Int) {
        if (viewHolder is ViewHolder) {
            val mServiceList = mProductList as List<ServicesDataResponse>
            viewHolder.bindServiceData(viewHolder as ViewHolder?, p1, mServiceList[p1], iAdapterClickListener)
        } else if (viewHolder is ViewHolder) {
            val mServiceList = mProductList as List<ServicesDataResponse>
            viewHolder.bindServiceData(viewHolder as ViewHolder?, p1, mServiceList[p1], iAdapterClickListener)
        }
    }

    private val VIEW_TYPE_ITEM = 0
    private val VIEW_TYPE_LOADING = 1
    private var isLoadingAdded = false
    private var retryPageLoad = false
    private var errorMsg: String? = ""

/*
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder? {
            val view = LayoutInflater.from(parent?.context).inflate(R.layout.exchange_service_product_list_row, parent, false)
            return ViewHolder(view)
        return null
    }
*/

/*
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder?, p1: Int) {
        if(viewHolder is ViewHolder){
            val mServiceList = mProductList as List<ServicesDataResponse>
            viewHolder.bindServiceData(viewHolder as ViewHolder?,p1, mServiceList[p1], iAdapterClickListener)
        }else if(viewHolder is ViewHolder){
            val mServiceList = mProductList as List<ServicesDataResponse>
            viewHolder.bindServiceData(viewHolder as ViewHolder?,p1, mServiceList[p1], iAdapterClickListener)
        }
    }
*/

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_reviews = view.tv_language_text!!
        fun bindServiceData(viewHolder: ViewHolder?, p1: Int, mServiceData: ServicesDataResponse,
                            iAdapterClickListener: IAdapterClick) {
            tv_reviews.text = mServiceData.title
            tv_reviews.onClick {
                iAdapterClickListener.cellItemClick("service", "Cell Item click", mServiceData, "", tv_reviews, p1)
            }
        }
    }

    override fun getItemCount(): Int {
        return if (mProductList == null) 0 else mProductList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == mProductList.size - 1 && isLoadingAdded) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }

    fun setElements(mProductList: MutableList<ServicesDataResponse?>, isOnRefreshLoaded: Boolean) {
        this.mProductList = mProductList as MutableList<Any?>
        notifyDataSetChanged()
    }

    fun addAll(moveResults: List<ServicesDataResponse>) {
        print("listSize:" + moveResults.size)
        for (result in moveResults) {
            add(result)
        }
    }

    fun remove(r: ServicesDataResponse) {
        val position = mProductList.indexOf(r)
        if (position > -1) {
            mProductList.remove(position)
            notifyItemRemoved(position)
        }
    }

    fun addLoadingFooter(totalPage: Int) {
        isLoadingAdded = totalPage != 1
        add(ServicesDataResponse())
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false
        val position = mProductList.size - 1
        val result = getItem(position)
        if (result != null) {
            mProductList.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun getItem(position: Int): ServicesDataResponse {
        return mProductList[position] as ServicesDataResponse
    }

    private fun add(result: ServicesDataResponse?) {
        mProductList.add(result)
        notifyItemInserted(mProductList.size - 1)
    }

    private fun removed(result: Any?) {
        mProductList.remove(result)
        notifyItemRemoved(mProductList.size - 1)
    }

    fun showRetry(show: Boolean, errorMsg: String?) {
        retryPageLoad = show
        notifyItemChanged(mProductList.size - 1)
        if (errorMsg != null) this.errorMsg = errorMsg
    }

    interface IAdapterClick {
        fun cellItemClick(mViewClickType: String, mString: String, cellRow: Any, mAttributeValue: String, mView: View, p1: Int): Unit
    }

    interface PaginationAdapterCallback {
        fun retryPageLoad()
    }
}