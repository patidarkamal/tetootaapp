package com.tetoota.proposal

import android.app.Activity
import com.tetoota.proposal.CheckProposalsDataResponce.CheckProposalsDataResponse
import com.tetoota.fragment.inbox.ProposalMessageData

/**
 * Created by jitendra.nandiya on 08-08-2017.
 */
class ProposalsContract {
    interface View {
        fun onProposalsApiSuccessResult(message: String?)
//        fun onProposalsExchangeApiSuccessResult(message: String?)
        fun onProposalsByIdSuccessResult(message: String?, proposalByIdData: ProposalByIdData?) {}
        fun onProposalActionSuccessREsult(message: String?, mProposalMesg: ProposalMessageData, acceptanceType: String, pos: Int) {}
        fun onProposalActionCancelSuccessREsult(message: String?, propoalId: String, acceptanceType: String, pos: Int) {}

        fun onProposalsFailureResult(message: String) {}
        fun onProposalsCancelFailureResult(message: String) {}
//        fun onProposalsExchangeFailureResult(message: String) {}
        fun onAddReviewApISuccessResult(message: String?) {}
        fun onAddReviewApiFailureResult(message: String?) {}
        fun onUserProposalsDataSuccess(checkProposalsDataResponse: CheckProposalsDataResponse?) {}
        fun onUserProposalsDataFailure(message: String?) {}
    }

    internal interface Presenter {
        fun getProposalsData(mActivity: Activity, postId: String, proposalTo: String, proposalFrom: String, tetootaPoints: String,
                             exchangePostId: String, exchangePostType: String, proposalTime: String)

        fun proposalAction(mActivity: Activity, propoalId: ProposalMessageData, acceptanceType: String, pos: Int) {}

       fun proposalActionCancel(mActivity: Activity, propoalId: String, acceptanceType: String, pos: Int) {}

        //        fun proposalCompleteAction(mActivity : Activity,propoalId : String, acceptanceType : String,pos : Int){}
        fun proposalCompleteAction(mActivity: Activity, propoalId: String, acceptanceType: String, receiver_id: String) {}

//        fun proposalExchangeCompleteAction(mActivity : Activity,propoalId : String, acceptanceType : String,receiver_id : String)
        fun getProposalById(proposalId: String, mActivity: Activity) {}

        fun addReviewApi(mActivity: Activity, mUserId: String,
                         mPostId: String, response_time: String, quality_services: String,
                         friendliness: String, review_message: String) {
        }

        fun checkProposal(mActivity: Activity, userId: String)
    }

    interface ProposalsApiListener {
        fun onProposalsAPiSuccess(message: String?)
        fun onProposalByIdSuccess(message: String?, proposalByIdData: ProposalByIdData?) {}
        fun onProposalActionSuccess(message: String?, mProposalMesg: ProposalMessageData, acceptanceType: String, pos: Int) {}
        fun onProposalCancelSuccess(message: String?, proposal_id : String, acceptanceType: String, pos: Int) {}
        fun onProposalCancelFailure(message: String?) {}
        fun onProposalsApiFailure(message: String)
        fun onAddReviewApISuccess(message: String?) {}
        fun onAddReviewApiFailure(message: String?) {}
        fun onCheckProposalsDataApiSuccess(checkProposalsDataResponseData: CheckProposalsDataResponse?) {}
        fun onCheckProposalsDataApiFailure(message: String?) {}
//        fun onProposalExchangeCompleteApiSuccess(message: String?)
//        fun onProposalsExchangeApiFailure(message: String)
    }
}
