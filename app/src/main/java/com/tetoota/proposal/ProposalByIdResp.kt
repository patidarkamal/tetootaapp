package com.tetoota.proposal
import com.tetoota.network.errorModel.Meta
data class ProposalByIdResp(
        val data: List<ProposalByIdData?>? = null,
        val meta: Meta? = null
)
