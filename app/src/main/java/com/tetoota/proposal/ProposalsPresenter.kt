package com.tetoota.proposal

import android.app.Activity
import com.tetoota.proposal.CheckProposalsDataResponce.CheckProposalsDataResponse
import com.tetoota.fragment.inbox.ProposalMessageData

class ProposalsPresenter : ProposalsContract.Presenter, ProposalsContract.ProposalsApiListener {

    override fun checkProposal(mActivity: Activity, userId: String) {
        mProposalsInteractor.checkProposalApiCall(mActivity, userId)
    }

    var mProposalsContract: ProposalsContract.View

    private val mProposalsInteractor: ProposalsInteractor by lazy {
        com.tetoota.proposal.ProposalsInteractor(this)
    }

    constructor(mHomeContract: ProposalsContract.View) {
        this.mProposalsContract = mHomeContract
    }

    override fun getProposalsData(mActivity: Activity, postId: String, proposalTo: String, proposalFrom: String, tetootaPoints: String,
                                  exchangePostId: String, exchangePostType: String, proposalTime: String) {
        mProposalsInteractor.proposalsApiCalling(mActivity, postId, proposalTo, proposalFrom, tetootaPoints, exchangePostId, exchangePostType, proposalTime)
    }

    override fun proposalAction(mActivity: Activity, mProposalMesgData: ProposalMessageData,
                                acceptanceType: String, pos: Int) {
        super.proposalAction(mActivity, mProposalMesgData, acceptanceType, pos)
        mProposalsInteractor.proposalActionApiCall(mActivity, mProposalMesgData, acceptanceType, pos)
    }

    override fun proposalActionCancel(mActivity: Activity, proposal_id: String,
                                acceptanceType: String, pos: Int) {
       // super.proposalActionCancel(mActivity, proposal_id , acceptanceType, pos)
        mProposalsInteractor.proposalActionCancelApiCall(mActivity, proposal_id, acceptanceType, pos)
    }

    override fun proposalCompleteAction(mActivity: Activity, mProposalId: String,
                                        acceptanceType: String, receiver_id: String) {
        super.proposalCompleteAction(mActivity, mProposalId, acceptanceType, receiver_id)
        if (!receiver_id.isEmpty()) {
            if(acceptanceType == "complete") {
                mProposalsInteractor.proposalExhangeDealApiCall(mActivity, mProposalId, "complete", receiver_id)
            }else{
                mProposalsInteractor.proposalExhangeDealApiCall(mActivity, mProposalId, "cancel", receiver_id)
            }
        } else {
            if (acceptanceType == "complete") {
                mProposalsInteractor.proposalDealApiCall(mActivity, mProposalId, "complete")
            } else {
                mProposalsInteractor.proposalDealApiCall(mActivity, mProposalId, "cancel")
            }
        }
    }
    /*override fun proposalExchangeCompleteAction(mActivity: Activity, mProposalId: String, acceptanceType: String, receiver_id: String) {
        super.proposalExchangeCompleteAction(mActivity, mProposalId, acceptanceType, receiver_id)
        if(acceptanceType == "complete"){
            mProposalsInteractor.proposalDealApiCall(mActivity,mProposalId,"complete",pos)
        }else{
            mProposalsInteractor.proposalDealApiCall(mActivity,mProposalId,"cancel",pos)
        }
    }*/

    /*override fun proposalExchangeCompleteAction(mActivity: Activity,mProposalId : String,
                                        acceptanceType: String,pos : String) {
        super.proposalExchangeCompleteAction(mActivity, mProposalId, acceptanceType,pos)
        if(acceptanceType == "complete"){
            mProposalsInteractor.proposalDealApiCall(mActivity,mProposalId,"complete",pos)
        }else{
            mProposalsInteractor.proposalDealApiCall(mActivity,mProposalId,"cancel",pos)
        }
    }*/

    override fun addReviewApi(mActivity: Activity, mUserId: String,
                              mPostId: String, response_time: String, quality_services: String,
                              friendliness: String, review_message: String) {
        super.addReviewApi(mActivity, mUserId, mPostId, response_time, quality_services, friendliness,
                review_message)
        mProposalsInteractor.addReviewApi(mActivity, mUserId, mPostId, response_time, quality_services, friendliness,
                review_message)
    }

    override fun getProposalById(proposalId: String, mActivity: Activity) {
        super.getProposalById(proposalId, mActivity)
        mProposalsInteractor.getProposalById(proposalId, mActivity)
    }

    override fun onProposalsAPiSuccess(message: String?) {
        mProposalsContract.onProposalsApiSuccessResult(message)
    }

    override fun onCheckProposalsDataApiFailure(message: String?) {
        super.onCheckProposalsDataApiFailure(message)
        mProposalsContract.onUserProposalsDataFailure(message)
    }

    override fun onCheckProposalsDataApiSuccess(checkProposalsDataResponse: CheckProposalsDataResponse?) {
        super.onCheckProposalsDataApiSuccess(checkProposalsDataResponse)
        mProposalsContract.onUserProposalsDataSuccess(checkProposalsDataResponse)
    }

    override fun onProposalActionSuccess(message: String?, mProposalMesg: ProposalMessageData, acceptanceType: String, pos: Int) {
        mProposalsContract.onProposalActionSuccessREsult(message, mProposalMesg, acceptanceType, pos)
    }

    override fun onProposalsApiFailure(message: String) {
        mProposalsContract.onProposalsFailureResult(message)
    }

    override fun onProposalCancelSuccess(message: String?, proposal_id: String, acceptanceType: String, pos: Int) {
        mProposalsContract.onProposalActionCancelSuccessREsult(message, proposal_id, acceptanceType, pos)
    }

    override fun onProposalCancelFailure(message: String?) {
        mProposalsContract.onProposalsCancelFailureResult(message!!)
    }


    override fun onProposalByIdSuccess(message: String?, proposalByIdData: ProposalByIdData?) {
        super.onProposalByIdSuccess(message, proposalByIdData)
        mProposalsContract.onProposalsByIdSuccessResult(message, proposalByIdData)
    }

    override fun onAddReviewApISuccess(message: String?) {
        super.onAddReviewApISuccess(message)
        mProposalsContract.onAddReviewApISuccessResult(message)
    }

    override fun onAddReviewApiFailure(message: String?) {
        super.onAddReviewApiFailure(message)
        if (message != null) {
            mProposalsContract.onProposalsFailureResult(message)
        }
    }
}
