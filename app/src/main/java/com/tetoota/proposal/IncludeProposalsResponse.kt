package com.tetoota.proposal
import com.tetoota.network.errorModel.Meta
data class IncludeProposalsResponse(
	val data: List<String?>? = null,
	val meta: Meta? = null
)
