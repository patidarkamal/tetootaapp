package com.tetoota.otp
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_otp.*
import kotlinx.android.synthetic.main.otp_passcode_layout.*
import org.jetbrains.anko.onClick

class OtpActivity : BaseActivity(),TextWatcher, View.OnKeyListener, View.OnFocusChangeListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)
        getValueFromPreviousScreen()
        tv_resend_otp.onClick {

        }
        tv_usercontactnumber.onClick {
            finish()
        }
    }

    private fun getValueFromPreviousScreen() {
        tv_usercontactnumber.text = intent.getStringExtra(INTENT_CONTACT_NUMBER)
        pin_hidden_edittext.addTextChangedListener(this@OtpActivity)
        pin_first_edittext.onFocusChangeListener = this
        pin_second_edittext.onFocusChangeListener = this
        pin_third_edittext.onFocusChangeListener = this
        pin_forth_edittext.onFocusChangeListener = this
        pin_first_edittext.setOnKeyListener(this)
        pin_second_edittext.setOnKeyListener(this)
        pin_third_edittext.setOnKeyListener(this)
        pin_forth_edittext.setOnKeyListener(this)
        pin_hidden_edittext.setOnKeyListener(this)
    }

    override fun afterTextChanged(s: Editable?) {
    }

    /**
     *
     */
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    /**
     *  on Text changed Override Method
     */
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        setDefaultPinBackground(pin_first_edittext)
        setDefaultPinBackground(pin_second_edittext)
        setDefaultPinBackground(pin_third_edittext)
        setDefaultPinBackground(pin_forth_edittext)
        if (s?.length == 0) {
            pin_first_edittext.setText("")
        } else if (s?.length == 1) {
            setFocusedPinBackground(pin_first_edittext)
            pin_first_edittext.setText(s[0] + "")
            pin_second_edittext.setText("")
            pin_third_edittext.setText("")
            pin_forth_edittext.setText("")
        } else if (s?.length == 2) {
            setFocusedPinBackground(pin_first_edittext)
            setFocusedPinBackground(pin_second_edittext)
            pin_second_edittext.setText(s[1] + "")
            pin_third_edittext.setText("")
            pin_forth_edittext.setText("")
        } else if (s?.length == 3) {
            setFocusedPinBackground(pin_first_edittext)
            setFocusedPinBackground(pin_second_edittext)
            setFocusedPinBackground(pin_third_edittext)
            pin_third_edittext.setText(s[2] + "")
            pin_forth_edittext.setText("")
        } else if (s?.length == 4) {
            setFocusedPinBackground(pin_first_edittext)
            setFocusedPinBackground(pin_second_edittext)
            setFocusedPinBackground(pin_third_edittext)
            setFocusedPinBackground(pin_forth_edittext)
            pin_forth_edittext.setText(s[3] + "")
            //on last digit OTP enter move to next screen
            if (Utils.haveNetworkConnection(this)) {
               Handler().postDelayed(Runnable {
                   verifyOTP()
               },100)
            } else {
                // TODO
            }
        }
    }

    /**
     * Verify User OTP
     */
    private fun verifyOTP() {
        val mStringBuilder = StringBuilder()
        mStringBuilder.append(pin_first_edittext.text.toString().trim())
        mStringBuilder.append(pin_second_edittext.text.toString().trim())
        mStringBuilder.append(pin_third_edittext.text.toString().trim())
        mStringBuilder.append(pin_forth_edittext.text.toString().trim())
        if(mStringBuilder.toString() == "1234"){
            hideSoftKeyboard(pin_forth_edittext)
         //   mOtpPresenter.otpAuthnetication(this@OtpActivity)
        }else{
            hideSoftKeyboard(pin_forth_edittext)
            pin_forth_edittext.setText("")
            pin_first_edittext.setText("")
            pin_second_edittext.setText("")
            pin_third_edittext.setText("")
            pin_hidden_edittext.setText("")
            setFocus(pin_hidden_edittext)
            showSnackBar("Invalid Otp")
        }
    }


    /**
     *
     */
    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if (event?.action == KeyEvent.ACTION_DOWN) {
            when (v?.id) {
                R.id.pin_hidden_edittext ->
                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        if (pin_hidden_edittext.text.length == 4)
                            pin_forth_edittext.setText("")
                        else if (pin_hidden_edittext.text.length == 3)
                            pin_third_edittext.setText("")
                        else if (pin_hidden_edittext.text.length == 2)
                            pin_second_edittext.setText("")
                        else if (pin_hidden_edittext.text.length == 1)
                            pin_first_edittext.setText("")
                        if (pin_hidden_edittext.length() > 0)
                            pin_hidden_edittext.setText(pin_hidden_edittext.text.subSequence(0, pin_hidden_edittext.length() - 1))
                        return true
                    }
                else ->  return false

            }
        }
        return false
    }

    /**
     * On focus Change
     */
    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        when(v?.id){
            R.id.pin_first_edittext ->
                if (hasFocus){
                    setFocus(pin_hidden_edittext)
                    showSoftKeyboard(pin_hidden_edittext)
                }
            R.id.pin_second_edittext ->
                if (hasFocus){
                    setFocus(pin_hidden_edittext)
                    showSoftKeyboard(pin_hidden_edittext)
                }
            R.id.pin_third_edittext ->
                if (hasFocus){
                    setFocus(pin_hidden_edittext)
                    showSoftKeyboard(pin_hidden_edittext)
                }
            R.id.pin_forth_edittext ->
                if (hasFocus){
                    setFocus(pin_hidden_edittext)
                    showSoftKeyboard(pin_hidden_edittext)
                }
        }
    }

    /**
     * Method to Show Soft Keyboard
     */
    private fun  showSoftKeyboard(editText: EditText?) {
        val imm = getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(editText, 0)
    }

    /**
     * Hide Soft Keyboard
     */
    fun hideSoftKeyboard(editText: EditText?) {
        val imm = getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(editText?.windowToken, 0)
    }

    /**
     * Method to Set focus
     */
    private fun  setFocus(editText: EditText?) {
        editText?.requestFocus()
        editText?.isFocusable = true
        editText?.isFocusableInTouchMode = true
    }

    /**
     * Sets default PIN background.

     * @param editText edit text to change
     */
    private fun setDefaultPinBackground(editText: EditText) {
        setViewBackground(editText, ContextCompat.getDrawable(this, R.drawable.edit_text_grey_bg))
    }

    /**
     * Sets focused PIN field background.

     * @param editText edit text to change
     */
    private fun setFocusedPinBackground(editText: EditText) {
        setViewBackground(editText, ContextCompat.getDrawable(this, R.drawable.edit_text_bg))
        // setViewBackground(editText, getResources().getDrawable(R.drawable.edit_text_bg));
    }

    /**
     * Sets background of the view.
     * This method varies in implementation depending on Android SDK version.

     * @param view       View to which set background
     * *
     * @param background Background to set to view
     */
    fun setViewBackground(view: View?, background: Drawable?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view?.background = background
        } else {
            view?.setBackgroundDrawable(background)
        }
    }


    // Login Screen to OTP Activity
    companion object {
        private val INTENT_CONTACT_NUMBER = "contact_number"
        fun newIntent(context: Context, contactNumber: String): Intent? {
            val intent = Intent(context, OtpActivity::class.java)
            intent.putExtra(INTENT_CONTACT_NUMBER, contactNumber)
            return intent
        }
    }

}


