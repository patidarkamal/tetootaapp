package com.tetoota.reviews

import android.app.Activity
import com.tetoota.TetootaApplication
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by charchit.kasliwal on 19-06-2017.
 */
class ViewReviewInteractor  {
    var call : Call<ViewReviewsResponse>? = null
    var mReviewApiListener: ViewReviewsContract.ReviewApiListener
    constructor(mReviewApiListener: ViewReviewsContract.ReviewApiListener) {
        this.mReviewApiListener = mReviewApiListener
    }

    fun reviewApiCalling(mActivity : Activity,personData : LoginDataResponse): Unit{
        call = TetootaApplication.getHeader()
                .getReviewsList(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity),
                        personData.user_id!!)
        call!!.enqueue(object : Callback<ViewReviewsResponse> {
            override fun onResponse(call: Call<ViewReviewsResponse>?, response: Response<ViewReviewsResponse>?) {
                println("Success")
                var mWalkThroughData : ViewReviewsResponse? = response?.body()
                if(response?.code() == 200){
                    if(response.body()?.data?.size!! > 0){
                        mReviewApiListener.onReviewAPiSuccess(mWalkThroughData?.data as List<ViewReviewsDataResponse>, "success")
                    }else{
                        mReviewApiListener.onReviewApiFailure(response.body()?.meta?.message.toString(), false)
                    }
                }else{
                    mReviewApiListener.onReviewApiFailure(response?.body()?.meta?.message.toString(), true)
                }
            }
            override fun onFailure(call: Call<ViewReviewsResponse>?, t: Throwable?) {
                mReviewApiListener.onReviewApiFailure(t?.message.toString(), true)
            }
        })
    }



    fun reviewApiCallingById(mActivity: Activity, postId: String): Unit{
        call = TetootaApplication.getHeader()
                .reviewByPostId(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity),
                        postId)
        call!!.enqueue(object : Callback<ViewReviewsResponse> {
            override fun onResponse(call: Call<ViewReviewsResponse>?, response: Response<ViewReviewsResponse>?) {
                println("Success")
                var mWalkThroughData : ViewReviewsResponse? = response?.body()
                if(response?.code() == 200){
                    if(response.body()?.data?.size!! > 0){
                        mReviewApiListener.onReviewAPiSuccess(mWalkThroughData?.data as List<ViewReviewsDataResponse>, "success")
                    }else{
                        mReviewApiListener.onReviewApiFailure(response.body()?.meta?.message.toString(), false)
                    }
                }else{
                    mReviewApiListener.onReviewApiFailure(response?.body()?.meta?.message.toString(), true)
                }
            }
            override fun onFailure(call: Call<ViewReviewsResponse>?, t: Throwable?) {
                mReviewApiListener.onReviewApiFailure(t?.message.toString(), true)
            }
        })
    }
}