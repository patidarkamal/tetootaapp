package com.tetoota.reviews
import com.tetoota.network.errorModel.Meta
data class ViewReviewsResponse(
        val data: List<ViewReviewsDataResponse?>? = null,
        val meta: Meta? = null
)
