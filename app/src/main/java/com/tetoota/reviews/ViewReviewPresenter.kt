package com.tetoota.reviews

import android.app.Activity
import com.tetoota.login.LoginDataResponse


/**
 * Created by charchit.kasliwal on 19-06-2017.
 */
class ViewReviewPresenter : ViewReviewsContract.Presenter,ViewReviewsContract.ReviewApiListener{
    var mReviewContract: ViewReviewsContract.View

    private val mReviewInteractor : ViewReviewInteractor by lazy {
        ViewReviewInteractor(this)
    }

    constructor(mHomeContract: ViewReviewsContract.View) {
        this.mReviewContract = mHomeContract
    }

    override fun getViewReviewData(mActivity: Activity,personData : LoginDataResponse) {
        mReviewInteractor.reviewApiCalling(mActivity,personData)
    }
    override fun getViewReviewDataById(mActivity: Activity,postId : String) {
        mReviewInteractor.reviewApiCallingById(mActivity,postId)
    }

    override fun onReviewAPiSuccess(mDataList: List<ViewReviewsDataResponse>, message: String) {
        mReviewContract.onViewReviewApiSuccessResult(mDataList as ArrayList<ViewReviewsDataResponse>,message)
    }

    override fun onReviewApiFailure(message: String, isServerError: Boolean) {
        mReviewContract.onViewReviewFailureResult(message,isServerError)
    }
}