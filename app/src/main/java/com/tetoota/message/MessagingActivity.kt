package com.tetoota.message

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.proposal.ProposalByIdData
import com.tetoota.proposal.ProposalsContract
import com.tetoota.proposal.ProposalsPresenter
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.proposal.ViewProposalsActivity
import com.tetoota.fragment.inbox.InboxContract
import com.tetoota.fragment.inbox.InboxPresenter
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.inbox.adapter.ChatRoomAdapter
import com.tetoota.fragment.inbox.fragment.InboxFragment
import com.tetoota.fragment.inbox.pojo.ChatHistoryDataResponse
import com.tetoota.fragment.inbox.pojo.OneToOneChatDataResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import io.intercom.android.sdk.Intercom
import kotlinx.android.synthetic.main.activity_messaging.*
import kotlinx.android.synthetic.main.chatting_layout.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.toast

class MessagingActivity : BaseActivity(), View.OnClickListener, InboxContract.View, ProposalsContract.View {
    private var mChatHistoryData: ChatHistoryDataResponse? = null
    private var mProposalMesgData: ProposalMessageData? = null
    private var chatArrayList: ArrayList<OneToOneChatDataResponse>? = null
    private lateinit var personData: LoginDataResponse
    private lateinit var mChatAdapter: ChatRoomAdapter
    private var trading_status: String = ""
    private var proposal_status: String = ""
    private var receiver_trading_status: String = ""
    var register: MenuItem? = null
    //var proposal_id : String? = "";
//    var isMenuVisiable : Boolean = false
    private lateinit var mRegistrationBroadcastReceiver: BroadcastReceiver
    //    private lateinit var mProposal: ProposalByIdData
    private val mInboxPresenter: InboxPresenter by lazy {
        InboxPresenter(this@MessagingActivity)
    }

    private val mProposalPresenter: ProposalsPresenter by lazy {
        ProposalsPresenter(this@MessagingActivity)
    }

    override fun ongetAllProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
    }

    override fun ongetAllProposalFailure(message: String) {
    }

    override fun onChatMesgSuccess(message: String) {
        super.onChatMesgSuccess(message)
        val oneToOneChat = OneToOneChatDataResponse()
        if (intent.getStringExtra("isProposalAccepted") != null) {
            oneToOneChat.msg_send_user_id = personData.user_id
            oneToOneChat.msg_recieved_user_id = mChatHistoryData?.msg_recieved_user_id.toString()
            oneToOneChat.post_id = mChatHistoryData?.post_id.toString()
            oneToOneChat.proposal_id = ""
            oneToOneChat.chatMesgDateTime = (android.text.format.DateFormat.format("yyyy-MM-dd kk:mm:ss", java.util.Date()).toString())
            oneToOneChat.chatMessageId = ""
            oneToOneChat.chat_message = chatBoxText.text.toString()
            oneToOneChat.conver_id = ""

        } else {
            var reciverId = mProposalMesgData?.proposal_sender_id
            var postId = mProposalMesgData?.post_id
            oneToOneChat.msg_send_user_id = personData.user_id
            oneToOneChat.msg_recieved_user_id = reciverId
            oneToOneChat.post_id = postId
            oneToOneChat.proposal_id = ""
            oneToOneChat.chatMesgDateTime = (android.text.format.DateFormat.format("yyyy-MM-dd kk:mm:ss", java.util.Date()).toString())
            oneToOneChat.chatMessageId = ""

            if (chatBoxText.text.isNotEmpty()) {
                oneToOneChat.chat_message = chatBoxText.text.toString()
            } else {
                oneToOneChat.chat_message = "Your Proposal has been accepted"
            }
            oneToOneChat.conver_id = ""
        }
        hideProgressDialog()
        chatArrayList?.add(oneToOneChat)
        mChatAdapter.notifyDataSetChanged()
        if (mChatAdapter.itemCount > 1) {
            // scrolling to bottom of the recycler view
            chatList.layoutManager.smoothScrollToPosition(chatList, null, mChatAdapter.itemCount - 1)
        }
        chatBoxText.setText("")
//        if(proposal_id != null && !proposal_id.equals("")) {
//            InboxFragment.isAcceptedRejected = true
//            val intent = ViewProposalsActivity.newMainIntent(this@MessagingActivity)
//            intent?.putExtra("proposalId", proposal_id)
//            ActivityStack.getInstance(this@MessagingActivity)
//            startActivity(intent)
//            proposal_id = ""
//        }
    }

    override fun onChatMesgFailure(message: String) {
        super.onChatMesgFailure(message)
        hideProgressDialog()
        showSnackBar(message)
    }

    fun validation(): Boolean {
        if (chatBoxText.text.toString().trim().isEmpty()) {
            toast(Utils.getText(this, StringConstant.chat_message_alert))
            return false
        }
        return true
    }

    override fun onClick(p0: View?) {
        when (p0) {
            send_bt -> {
                if (validation()) {
                    if (com.tetoota.utility.Utils.haveNetworkConnection(this@MessagingActivity)) {
                        if (chatBoxText.text.toString().isNotEmpty()) {
                            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                            if (intent.getStringExtra("isProposalAccepted") != null) {
                                if (personData.user_id.toString() == mChatHistoryData?.msg_send_user_id.toString()) {
                                    mInboxPresenter.sendMesg(this@MessagingActivity,
                                            personData.user_id, mChatHistoryData?.msg_recieved_user_id.toString(),
                                            mChatHistoryData?.post_id!!, chatBoxText.text.toString().trim(), mChatHistoryData?.proposal_id!!)
                                } else if (personData.user_id.toString() == mChatHistoryData?.msg_recieved_user_id.toString()) {
                                    mInboxPresenter.sendMesg(this@MessagingActivity,
                                            personData.user_id, mChatHistoryData?.msg_send_user_id.toString(),
                                            mChatHistoryData?.post_id!!, chatBoxText.text.toString().trim(), mChatHistoryData?.proposal_id!!)
                                }
                            } else {
                                var reciverId = mProposalMesgData?.proposal_sender_id
                                var postId = mProposalMesgData?.post_id
                                mInboxPresenter.sendMesg(this@MessagingActivity,
                                        personData.user_id, reciverId!!,
                                        postId!!, chatBoxText.text.toString().trim(), mProposalMesgData?.proposal_id.toString())
                            }
                        } else {
                            hideProgressDialog()
                        }
                    } else {
                        hideProgressDialog()
                        showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                    }
                }
            }
            btn_acc -> {
                if (Utils.haveNetworkConnection(this@MessagingActivity)) {
                    showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                    //proposal_id = mProposalMesgData!!.proposal_id
                    mProposalPresenter.proposalAction(this@MessagingActivity, mProposalMesgData!!, "Accept", 0)
                } else {
                    showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                }
            }
            btn_dec -> {
                if (Utils.haveNetworkConnection(this@MessagingActivity)) {
                    showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                    mProposalPresenter.proposalAction(this@MessagingActivity,
                            mProposalMesgData!!, "Decline", 0)
                } else {
                }
            }
            btn_completeDeal -> {
                val intent = ViewProposalsActivity.newMainIntent(this@MessagingActivity)
                if (this.intent.getStringExtra("isProposalAccepted") != null) {
                    intent?.putExtra("proposalId", mChatHistoryData?.proposal_id)
                } else {
                    intent?.putExtra("proposalId", mProposalMesgData?.proposal_id)
                }
                ActivityStack.getInstance(this@MessagingActivity)
                startActivity(intent)
            }
        }
    }

    private fun sendMessageMethod() {
        if (com.tetoota.utility.Utils.haveNetworkConnection(this@MessagingActivity)) {
            if (intent.getStringExtra("isProposalAccepted") != null) {
                if (personData.user_id.toString() == mChatHistoryData?.msg_send_user_id.toString()) {
                    mInboxPresenter.sendMesg(this@MessagingActivity,
                            personData.user_id, mChatHistoryData?.msg_recieved_user_id.toString(),
                            mChatHistoryData?.post_id!!, chatBoxText.text.toString().trim(), mChatHistoryData?.proposal_id!!)
                } else if (personData.user_id.toString() == mChatHistoryData?.msg_recieved_user_id.toString()) {
                    mInboxPresenter.sendMesg(this@MessagingActivity,
                            personData.user_id, mChatHistoryData?.msg_send_user_id.toString(),
                            mChatHistoryData?.post_id!!, chatBoxText.text.toString().trim(), mChatHistoryData!!.proposal_id!!)
                }
            } else {
                var reciverId = mProposalMesgData?.proposal_sender_id
                var postId = mProposalMesgData?.post_id
                if (reciverId != "" && postId != "") {
                    mInboxPresenter.sendMesg(this@MessagingActivity,
                            personData.user_id, reciverId!!,
                            postId!!, "Your Proposal has been accepted", mProposalMesgData?.proposal_id.toString())
                }
            }
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_messaging)

        Intercom.client().handlePushMessage()
        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)
        send_bt.setOnClickListener(this)
        btn_acc.setOnClickListener(this)
        btn_dec.setOnClickListener(this)
        btn_completeDeal.visibility = View.GONE
        btn_completeDeal.setOnClickListener(this)
        println("Current time" + (android.text.format.DateFormat.format("yyyy-MM-dd kk:mm:ss", java.util.Date()).toString()))
        setMultiLanguageText()
        getDataFromPReviousScreen()
        mRegistrationBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, intent: Intent) {
                if (intent.getStringExtra("messageAlert") == "Message Alert") {
                    println("Message Push Received")
                    getDataFromPReviousScreen()
                }
            }
        }
    }

    private fun callProposalData(mPraposalId: String) {
        if (Utils.haveNetworkConnection(this@MessagingActivity)) {
            mProposalPresenter.getProposalById(mPraposalId, this@MessagingActivity)
        }
    }

    private fun getDataFromPReviousScreen() {
        val json = com.tetoota.utility.Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MessagingActivity)
        personData = Gson().fromJson(json, LoginDataResponse::class.java)
        chatArrayList = ArrayList<OneToOneChatDataResponse>()
        val layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = true
        chatList.layoutManager = layoutManager
        /*   chatList.layoutManager = LinearLayoutManager(this@MessagingActivity)*/
        chatList.setHasFixedSize(true)
        if (intent.getStringExtra("isProposalAccepted") != null) {
            cv_main.visibility = View.GONE
            mChatHistoryData = intent.getParcelableExtra<ChatHistoryDataResponse>("inboxPojo")
            fetchChatHistory(personData, mChatHistoryData!!)
            if (personData.user_id == mChatHistoryData!!.msg_send_user_id.toString()) {
                initToolbar(mChatHistoryData!!.msg_reciever_name!!)
            } else {
                initToolbar(mChatHistoryData!!.msg_sender_name!!)
            }
        } else {
            mProposalMesgData = intent.getParcelableExtra<ProposalMessageData>("proposalMesgData")
            cv_main.visibility = View.VISIBLE
            tv_reply.visibility = View.GONE
            tv_time.visibility = View.GONE
          //  tv_title.text = mProposalMesgData!!.title
            tv_username.text = "${mProposalMesgData!!.proposal_from_user_firstName.toString()} ${mProposalMesgData!!.proposal_from_user_lastName.toString()}"
            initToolbar("${mProposalMesgData!!.proposal_from_user_firstName.toString()} ${mProposalMesgData!!.proposal_from_user_lastName.toString()}")

/*
            if (mProposalMesgData!!.exchange_post_type == null || mProposalMesgData!!.exchange_post_type == "" )
            {
               // Log.e("iffffff","" + mProposalMesgData!!.exchange_post_type)
                tv_proposal_desc.text = (mProposalMesgData!!.exchange_post_title +" Tetoota points")
            }else{
               // Log.e("elseeee","" + mProposalMesgData!!.exchange_post_type)
                tv_proposal_desc.text = mProposalMesgData!!.exchange_post_title
            }
*/

            if (mProposalMesgData!!.exchange_post_id == "0") {
                //Log.e("iffffff","" + mProposalMesg)
                var iwant = Utils.getText(this, StringConstant.str_tv_i_want)
                var iwill = Utils.getText(this, StringConstant.str_tv_will_offer)
                var sss = "<font color='#3a3a3a'> " + iwill + " </font> " + mProposalMesgData!!.exchange_post_title + " Tetoota points"
                var within = "<font color='#3a3a3a'> " + mProposalMesgData!!.proposal_time + "</font> "
                tv_title.text = (Html.fromHtml("<font color='#3a3a3a'>" + iwant + "</font> " + mProposalMesgData!!.title + sss + within))
                // tv_proposal_desc.text = (mProposalMesgData!!.exchange_post_title + " Tetoota points")
            } else {
                var iwant = Utils.getText(this, StringConstant.str_tv_i_want)
                var iwill = Utils.getText(this, StringConstant.str_tv_will_offer)
                var sss = "<font color='#3a3a3a'> " + iwill + " </font> " + mProposalMesgData!!.exchange_post_title
                var within = "<font color='#3a3a3a'> " + mProposalMesgData!!.proposal_time + "</font> "
                tv_title.text = (Html.fromHtml("<font color='#3a3a3a'>" + iwant + "</font> " + mProposalMesgData!!.title + sss + within))
                // tv_proposal_desc.text = mProposalMesgData!!.exchange_post_title
            }
            // tv_proposal_desc.text = mProposalMesgData!!.exchange_post_title.toString()

            //  tv_with_in.text = mProposalMesgData!!.proposal_time.toString()

            //    tv_timeleft.text = "You have ${mProposalMesgData!!.hours_left} to except this proposal"

            val str = Utils.getText(this, StringConstant.proposal_time_left)
            val strReplace = str.replace("%d", "%s")
            tv_timeleft.text = String.format(strReplace, mProposalMesgData?.hours_left)


            if (mProposalMesgData!!.proposal_from_user_profile_img!!.isEmpty()) {
                iv_user_pic.setImageResource(R.drawable.user_placeholder);
            } else{
                Picasso.get().load(mProposalMesgData!!.proposal_from_user_profile_img!!).into(iv_user_pic)
            }

/*
            Glide.with(this@MessagingActivity)
                    .load(Utils.getUrl(this@MessagingActivity, mProposalMesgData!!.proposal_from_user_profile_img!!))
                    .centerCrop()
                    .dontAnimate()
                    .placeholder(R.drawable.user_placeholder)
                    .error(R.drawable.user_placeholder)
                    .into(iv_user_pic)
*/

            var reciverId = mProposalMesgData!!.proposal_sender_id
            var postId = mProposalMesgData!!.post_id
            println("Receiver" + reciverId + "<=--->" + postId)
            fetchChat(personData.user_id.toString(),
                    mProposalMesgData!!.proposal_sender_id.toString(),personData.user_id.toString(), mProposalMesgData!!.post_id.toString())
        }
    }

    /**
     * the m Proposal PushNotificationDataResponse
     */
    private fun initToolbar(mproPosalData: String): Unit {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar_title.text = mproPosalData
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, MessagingActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.mesg_screen_menu, menu)
        register = menu?.findItem(R.id.action_view_proposal)?.setIcon(R.drawable.complete_deal)
        register?.isVisible = false
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        /* register = menu?.findItem(R.id.action_view_proposal)
         register?.isVisible = true
         return super.onPrepareOptionsMenu(menu)*/
        menu?.clear()
        menuInflater.inflate(R.menu.mesg_screen_menu, menu)
        register = menu?.findItem(R.id.action_view_proposal)?.setIcon(R.drawable.complete_deal)
        register?.isVisible = false
        /*if (isMenuVisiable){
            register?.isVisible = true
        }else{
            register?.isVisible = false
        }*/
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_view_proposal) {
//            val intent = ViewProposalsActivity.newMainIntent(this@MessagingActivity)
//            if (this.intent.getStringExtra("isProposalAccepted") != null) {
//                intent?.putExtra("proposalId", mChatHistoryData?.proposal_id)
//            }else{
//                intent?.putExtra("proposalId", mProposalMesgData?.proposal_id)
//            }
//            ActivityStack.getInstance(this@MessagingActivity)
//            startActivity(intent)
            return true
        } else if (id == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Utils.hideSoftKeyboard(this@MessagingActivity)
        ActivityStack.removeActivity(this@MessagingActivity)
        finish()
    }

    override fun onOneToOneSuccess(message: String, mChatList: ArrayList<OneToOneChatDataResponse>) {
        super.onOneToOneSuccess(message, mChatList)
        if (mChatList.size > 0) {
            chatArrayList = mChatList
//            trading_status = chatArrayList!![0].trading_status.toString()
//            register?.isVisible = trading_status != "complete"
//            callProposalData(chatArrayList!![0].proposal_id.toString())
            if (intent.getStringExtra("isProposalAccepted") != null) {
                callProposalData(mChatHistoryData?.proposal_id.toString())
            } else {
                callProposalData(mProposalMesgData?.proposal_id.toString())
            }
            mChatAdapter = ChatRoomAdapter(this@MessagingActivity, personData, chatArrayList!!)
            chatList.adapter = mChatAdapter
        } else {
            chatArrayList = mChatList
            mChatAdapter = ChatRoomAdapter(this@MessagingActivity, personData, chatArrayList!!)
            chatList.adapter = mChatAdapter
//            register?.isVisible = false
        }
    }

    override fun onOneToOneFailure(message: String) {
        super.onOneToOneFailure(message)
        showSnackBar(message)
    }

    /**
     * Fetch Chat History
     */
    private fun fetchChatHistory(personData: LoginDataResponse, mProposalData: ChatHistoryDataResponse) {
        if (com.tetoota.utility.Utils.haveNetworkConnection(this@MessagingActivity)) {
            mInboxPresenter.getOneToOneChatData(this@MessagingActivity, mProposalData.msg_send_user_id.toString(),
                    mProposalData.msg_recieved_user_id.toString(),personData.user_id.toString(), mProposalData.post_id.toString())
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    /**
     * Fetch Chat History
     */
    private fun fetchChat(msg_send_user_id: String, msg_recieved_user_id: String,user_id:String,
                          post_id: String) {
        if (com.tetoota.utility.Utils.haveNetworkConnection(this@MessagingActivity)) {
            mInboxPresenter.getOneToOneChatData(this@MessagingActivity, msg_send_user_id,
                    msg_recieved_user_id,user_id, post_id)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    override fun onResume() {
        super.onResume()
        TetootaApplication.activityResumed()
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                IntentFilter(Constant.PUSH_NOTIFICATION))
    }

    override fun onPause() {
        TetootaApplication.activityPaused()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver)
        super.onPause()
    }

    override fun onProposalsApiSuccessResult(message: String?) {
        if (message != null) {
            cv_main.visibility = View.GONE
            showSnackBar(message)
            // jitu
        }
    }

    override fun onProposalsByIdSuccessResult(message: String?, proposalByIdData: ProposalByIdData?) {
        super.onProposalsByIdSuccessResult(message, proposalByIdData)
        if (proposalByIdData != null) {
//            if (personData.user_id != proposalByIdData.proposal_from.toString()) {
            var oneToOneChatDataResponse: OneToOneChatDataResponse? = chatArrayList!![chatArrayList!!.size - 1]
            trading_status = oneToOneChatDataResponse?.trading_status.toString()
            proposal_status = oneToOneChatDataResponse?.proposal_status.toString()
            receiver_trading_status = oneToOneChatDataResponse?.receiver_trading_status.toString()

/*
            if(proposalByIdData.exchange_post_type?.isEmpty()!!){
                if (personData.user_id == proposalByIdData.proposal_from.toString()) {
                    if(trading_status.equals("Pending",true) && proposal_status.equals("Accept", true)) {
                        btn_completeDeal.visibility = View.VISIBLE
                    } else {
                        btn_completeDeal.visibility = View.GONE
                    }
                }
            }else{
                if (personData.user_id == proposalByIdData.proposal_from.toString()) {
                    if (trading_status.equals("Pending", true) && proposal_status.equals("Accept", true)) {
                        btn_completeDeal.visibility = View.VISIBLE
                    } else {
                        btn_completeDeal.visibility = View.GONE
                    }
                }else{
                    if (receiver_trading_status.equals("Pending", true) && proposal_status.equals("Accept", true)) {
                        btn_completeDeal.visibility = View.VISIBLE
                    } else {
                        btn_completeDeal.visibility = View.GONE
                    }
                }
            }
*/

            if (proposalByIdData.exchange_post_type?.isEmpty()!!) {
                if (personData.user_id == proposalByIdData.proposal_from.toString()) {
                    if (trading_status.equals("Pending", true) && proposal_status.equals("Accept", true)) {
                        btn_completeDeal.visibility = View.VISIBLE
                    } else {
                        btn_completeDeal.visibility = View.GONE
                    }
                } else {
                    if (trading_status.equals("complete", true) &&
                            proposal_status.equals("Accept", true)) {
                        btn_completeDeal.visibility = View.GONE
                    } else {
                        btn_completeDeal.visibility = View.GONE
                    }
                }
            } else {
                if (personData.user_id == proposalByIdData.proposal_from.toString()) {
                    if (trading_status.equals("Pending", true) && proposal_status.equals("Accept", true)) {
                        btn_completeDeal.visibility = View.VISIBLE
                    } else {
                        btn_completeDeal.visibility = View.GONE
                    }
                } else {
                    if (receiver_trading_status.equals("Pending", true) && proposal_status.equals("Accept", true)) {
                        btn_completeDeal.visibility = View.VISIBLE
                    } else {
                        btn_completeDeal.visibility = View.GONE
                    }
                }
            }


        }
    }

    override fun onProposalActionSuccessREsult(message: String?, mProposalMesg: ProposalMessageData, acceptanceType: String, pos: Int) {
        super.onProposalActionSuccessREsult(message, mProposalMesg, acceptanceType, pos)
        if (acceptanceType == "Accept") {
            cv_main.visibility = View.GONE
            showSnackBar("Proposal Accept")
            /*isMenuVisiable = true
            invalidateOptionsMenu()*/
            sendMessageMethod()
        } else {
            InboxFragment.isAcceptedRejected = true
            showSnackBar("Proposal Decline")
        }
    }

    override fun onProposalsFailureResult(message: String) {
        super.onProposalsFailureResult(message)
        if (message != null) {
            showSnackBar(message)
        }
    }

    private fun setMultiLanguageText() {
        btn_completeDeal.text = Utils.getText(this, StringConstant.complete_deal)
        tv_reply.text = Utils.getText(this, StringConstant.chat_reply)
        btn_acc.text = Utils.getText(this, StringConstant.accept)
        btn_dec.text = Utils.getText(this, StringConstant.decline)
       // var iwant = Utils.getText(this, StringConstant.str_tv_i_want)
       // var iwill = Utils.getText(this, StringConstant.str_tv_will_offer)
        chatBoxText.hint = Utils.getText(this, StringConstant.str_chatBoxText)
    }
}
