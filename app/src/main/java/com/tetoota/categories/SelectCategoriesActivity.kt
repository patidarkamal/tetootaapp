package com.tetoota.categories

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.text.SpannableString
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import com.facebook.accountkit.Account
import com.facebook.accountkit.AccountKit
import com.facebook.accountkit.AccountKitCallback
import com.facebook.accountkit.AccountKitError
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.fragment.help.ShowWebViewActivity
import com.tetoota.login.LoginDataResponse
import com.tetoota.main.MainActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_select_categories.*
import kotlinx.android.synthetic.main.error_layout.*

class SelectCategoriesActivity : BaseActivity(), View.OnClickListener, CategoriesContract.View,
        CategoriesAdapter.IAdapterClickListener, SelectedCategoriesAdapter.ISelectedAdapterClickListener {

    var mCategoriesResponse = ArrayList<CategoriesDataResponse>()
    var number: String? = null
    //    lateinit var languageMap: HashMap<String, String>
    var mSelectedCategoriesResponse = ArrayList<CategoriesDataResponse>()
    var isCheckboxChecked: Boolean = false
    lateinit var mCategoriesAdapter: CategoriesAdapter
    lateinit var mSelectedCategoriesAdapter: SelectedCategoriesAdapter

    var gridLayoutManager: GridLayoutManager? = null
    private val mCategoriesPresenter: CategoriesPresenter by lazy {
        CategoriesPresenter(this@SelectCategoriesActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_categories1)
        initViews()
        btn_next.setOnClickListener(this@SelectCategoriesActivity)
        error_btn_retry.setOnClickListener(this@SelectCategoriesActivity)
        //checkbox_layout.setOnClickListener(this@SelectCategoriesActivity)
        tv_terms_text.setOnClickListener(this@SelectCategoriesActivity)
        //  chkbx_terms.setOnClickListener(this@SelectCategoriesActivity)
    }

    private fun initViews() {
        mCategoriesAdapter = com.tetoota.categories.CategoriesAdapter(iAdapterClickListener = this)
        mSelectedCategoriesAdapter = com.tetoota.categories.SelectedCategoriesAdapter(iAdapterClickListener = this)
        gridLayoutManager = GridLayoutManager(this, 2)
       // rl_category_list.layoutManager = LinearLayoutManager(this@SelectCategoriesActivity)
        rl_category_list.layoutManager = gridLayoutManager
        rl_category_list.setHasFixedSize(false)
        rl_category_list.adapter = mCategoriesAdapter

       //  rl_selected_category.setLayoutManager(LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true))
       //  rl_selected_category.adapter = mSelectedCategoriesAdapter

        AccountKit.getCurrentAccount(object : AccountKitCallback<Account> {
            override fun onSuccess(account: Account) {
                val string: String = account.phoneNumber.phoneNumber
                number = account.phoneNumber.phoneNumber
                Log.e("number+++++++", "" + number)
                println("Phone Number $string")
                // showSnackBar(number.toString())
            }

            override fun onError(error: AccountKitError) {
                showSnackBar(error.errorType.message)
            }
        })
        setMultiLanguageText()
    }

    private fun setMultiLanguageText() {
        tv_select_categories.text = Utils.getText(this, StringConstant.str_please_select_categories)
        btn_next.text = Utils.getText(this, StringConstant.str_next)
        val tempString = Utils.getText(this, StringConstant.help_term_condition)
        val spanString = SpannableString(tempString)
        spanString.setSpan(UnderlineSpan(), 0, spanString.length, 0)
        spanString.setSpan(StyleSpan(Typeface.BOLD), 0, spanString.length, 0)
        spanString.setSpan(StyleSpan(Typeface.ITALIC), 0, spanString.length, 0)
        tv_terms_text.text = spanString
    }

    override fun onClick(v: View?) {
        when (v) {
            error_btn_retry -> {
                if (Utils.haveNetworkConnection(this@SelectCategoriesActivity)) {
                    showProgressDialog("${Utils.getText(this, StringConstant.please_wait)}")
                    mCategoriesPresenter.getCategoriesData(this@SelectCategoriesActivity)
                } else {
                    showErrorView("${Utils.getText(this, StringConstant.str_check_internet)}")
                }
            }
            btn_next -> {
                if (Utils.haveNetworkConnection(this@SelectCategoriesActivity)) {
                    val result = StringBuilder()
                    if (mSelectedCategoriesResponse.size > 0) {
                        /*if (isCheckboxChecked) {
                            showProgressDialog("${Utils.getText(this, StringConstant.please_wait)}")

                        } else {
                            showSnackBar(Utils.getText(this, StringConstant.term_condition_message_alert))
                        }*/

                        for (item in mSelectedCategoriesResponse.indices) {
                            result.append(mSelectedCategoriesResponse[item].category_id)
                            result.append(",")
                        }
                        if (result.endsWith(",")) {
                            var resultnew = result.substring(0, result.length - 1)
                            println("Result $result $resultnew")
                            val referral_string = Utils.loadPrefrence(Constant.KEY_NAME, "", this)
                           // Log.e("referral_string ","**** " + referral_string)
                            mCategoriesPresenter.checkUserLoginFromAPI(this@SelectCategoriesActivity, number.toString(), resultnew, referral_string)
                        }
                    } else {
                        showSnackBar(Utils.getText(this, StringConstant.add_service_category_alert))
                    }
                } else {
                    showErrorView("${Utils.getText(this, StringConstant.str_check_internet)}")
                }
            }
            tv_terms_text -> {
                val intent = ShowWebViewActivity.newMainIntent(this@SelectCategoriesActivity)
                ActivityStack.getInstance(this@SelectCategoriesActivity)
                //intent?.putExtra("webUrl", webPageUrl)
                intent?.putExtra("pageHeading", "Terms and Conditions")
                startActivity(intent)
            }
/*
            chkbx_terms -> {
                if (isCheckboxChecked) {
                    isCheckboxChecked = false
                    chkbx_terms.setBackgroundDrawable(resources.getDrawable(R.drawable.iv_unchek_box))
                } else {

                    isCheckboxChecked = true
                    chkbx_terms.setBackgroundDrawable(resources.getDrawable(R.drawable.iv_chek_box))


                   */
/* val alertDialog = AlertDialog.Builder(this@SelectCategoriesActivity) //Read Update
                    alertDialog.setMessage(Utils.getText(this, StringConstant.terms_conditions_accept_alert))
                    alertDialog.setPositiveButton(Utils.getText(this, StringConstant.tetoota_ok)
                            , object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface, which: Int) {
                            isCheckboxChecked = true
                            chkbx_terms.setBackgroundDrawable(resources.getDrawable(R.drawable.iv_chek_box))
                            dialog.cancel()
                        }
                    })
                    alertDialog.setNegativeButton(Utils.getText(this, StringConstant.cancel)
                            , object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface, which: Int) {
                            dialog.cancel()
                        }
                    })
                    alertDialog.show() *//*
 //<-- See This!
                }
            }
*/
        }
    }

    /**
     * On Resume
     */
    override fun onResume() {
        super.onResume()
        if (Utils.haveNetworkConnection(this@SelectCategoriesActivity)) {
            showProgressDialog("${Utils.getText(this, StringConstant.please_wait)}")
            mCategoriesPresenter.getCategoriesData(this@SelectCategoriesActivity)
        } else {
            showErrorView("${Utils.getText(this, StringConstant.str_check_internet)}")
        }
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, SelectCategoriesActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onCategoriesSuccessResult(mCategoriesList: List<CategoriesDataResponse>, message: String) {
        hideProgressDialog()
        hideErrorView()
        // showSnackBar(message)
        this.mCategoriesResponse = mCategoriesList as ArrayList<CategoriesDataResponse>
        mCategoriesAdapter.setElements(mCategoriesList)
        mCategoriesAdapter.notifyDataSetChanged()
        rl_category_list.visibility = View.VISIBLE
    }

    override fun onCatergoriesFailureResult(message: String) {
        hideProgressDialog()
        showErrorView(message)
        showSnackBar(message)
    }

    override fun cellItemClick(mString: Int, cellRow: Any) {
        var mCategoryData = cellRow as CategoriesDataResponse
        if (!mCategoryData.isSelected) {
            mCategoryData.isSelected = true
            mSelectedCategoriesResponse.add(mCategoryData)
        } else {
            mCategoryData.isSelected = false
            mSelectedCategoriesResponse.remove(mCategoryData)
        }
        notifyAdapter()
    }

    override fun cellSelectedItemClick(mString: Int, cellRow: Any) {
        var mCategoryData = cellRow as CategoriesDataResponse
        mCategoryData.isSelected = false
        mSelectedCategoriesResponse.remove(mCategoryData)
        notifyAdapter()
    }

    fun notifyAdapter() {
        mSelectedCategoriesAdapter.setElements(mSelectedCategoriesResponse)
        mSelectedCategoriesAdapter.notifyDataSetChanged()
        mCategoriesAdapter.setElements(mCategoriesResponse)
        mCategoriesAdapter.notifyDataSetChanged()
    }

    override fun onLoginResult(message: Int, mesgDesc: String) {
        hideProgressDialog()
        if (message == Constant.API_SUCCESS_VALUE) {
            if (Utils.haveNetworkConnection(this)) {
                showProgressDialog("${Utils.getText(this, StringConstant.please_wait)}")
                val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                // val userAgent = System.getProperty("http.agent")
                //  Log.e("SSSSSSSSSSSSSSSSSSSS ","" + userAgent)
                // Log.e("Utils.getIPAddress ","" + Utils.getIPAddress(true))
                mCategoriesPresenter.doDeepLinking(this@SelectCategoriesActivity, Utils.loadPrefrence(Constant.USER_AUTH_TOKEN
                        , "", this@SelectCategoriesActivity), personData.user_id!!
                        , Utils.getIPAddress(true))
            } else {
                moveToNextScreen()
            }
        } else if (message == Constant.API_FAILURE_VALUE) {
            println("Failure")
        } else {
            println("Unkknown")
        }
    }

    override fun onDeepLinkingResult() {

        //  Log.e("fffffffffffffffffffffffffffff","")
        hideProgressDialog()
        moveToNextScreen()
    }

    fun moveToNextScreen() {
        var mobile_number = number.toString()
        val intent = MainActivity.newMainIntent(this)
        ActivityStack.getInstance(this@SelectCategoriesActivity)
        overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
        ActivityStack.cleareAll()
        startActivity(intent)
        finish()
        println("Success")
    }

    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            rl_maincontent.visibility = View.VISIBLE
            checkbox_layout.visibility = View.VISIBLE
        }
    }

    /**
     * @param throwable required for [.fetchErrorMessage]
     * *
     * @return
     */
    private fun showErrorView(throwable: String) {
        if (error_layout.visibility === View.GONE) {
            error_layout.visibility = View.VISIBLE
            error_txt_cause.text = throwable
            rl_maincontent.visibility = View.GONE
            checkbox_layout.visibility = View.GONE
        }
    }
}
