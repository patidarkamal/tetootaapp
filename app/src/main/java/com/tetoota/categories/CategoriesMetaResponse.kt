package com.tetoota.categories

/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
data class CategoriesMetaResponse(var code: String, var status : String , var message : String)
