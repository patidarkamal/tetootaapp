package com.tetoota.categories



/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
data class CategoriesResponse(var meta : CategoriesMetaResponse?,
    val data : List<CategoriesDataResponse>)