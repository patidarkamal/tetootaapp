package com.tetoota.selectlanguage
import android.app.Activity
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.tetoota.TetootaApplication
import com.tetoota.database.DBAdapter
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by charchit.kasliwal on 01-06-2017.
 */
class SelectLanguageInteractor {
    var mSelectApiListener: SelectLanguageContract.ApiListener

    constructor(mSelectApiListener: SelectLanguageContract.ApiListener) {
        this.mSelectApiListener = mSelectApiListener
    }

    fun getLanguageDataFromApi(){
        var call: Call<SelectLanguageResponse> = TetootaApplication.getHeader()
                .getLanguageList(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD)
        call.enqueue(object : Callback<SelectLanguageResponse> {
            override fun onResponse(call: Call<SelectLanguageResponse>?,
                                    response: Response<SelectLanguageResponse>?) {
                var mLanguageListData: SelectLanguageResponse? = response?.body()
                println("UserVisibilityResponse Code ${response?.code()}")
                if(response?.code() == 200){
                    mSelectApiListener.onLanguageApiSuccess(mLanguageListData?.data!!,"success")
                }else{
                    mSelectApiListener.onLanguageApiFailure(response?.body()?.meta?.message.toString())
                }
            }
            override fun onFailure(call: Call<SelectLanguageResponse>?, t: Throwable?) {
                println("Review API RESPONSE Failure ${t?.message.toString()}")
                mSelectApiListener.onLanguageApiFailure(t?.message.toString())
            }
        })
    }

    /**
     * Method to Get Selected Language pushNotificationDataResponse
     */
    fun getSelectedLanguageData(mActivity : Activity){
        var call: Call<JsonObject> = TetootaApplication.getHeader()
                .getSelectedLanguageData(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD
                        , Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity), "2017-07-23 05:16:47")
                      //  (android.text.format.DateFormat.format("yyyy-MM-dd kk:mm:ss", java.util.Date()).toString()))
        println("Current DAte and time ${(android.text.format.DateFormat.format("yyyy-MM-dd kk:mm:ss", java.util.Date()).toString())}")
        call.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>?,
                                    response: Response<JsonObject>?) {
                println("UserVisibilityResponse Code ${response?.code()}")
                if(response?.code() == 200){
                    var jsonObj: JsonObject = response.body() as JsonObject
                    var databaseHelper: DBAdapter = DBAdapter(mActivity)
                        var languageObj: JsonArray = jsonObj.getAsJsonArray("data")
                        val collectionType = object : TypeToken<MutableList<SelectedLanguageDataResponse>>() {}.type
                        val languagesList: MutableList<SelectedLanguageDataResponse> = Gson().fromJson(languageObj, collectionType)
                        if (languagesList.size > 0) {
                            databaseHelper.open()
                            databaseHelper.insertLanguage(languagesList)
                            databaseHelper.close()
                        }

                    mSelectApiListener.onSelectedLanguageSuccess("success")
                }else{
                    mSelectApiListener.onSelectedLanguageSuccess("failure")
                }
            }
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                println("Review API RESPONSE Failure ${t?.message.toString()}")
                mSelectApiListener.onSelectedLanguageSuccess("${t?.message.toString()}")
            }
        })
    }
}