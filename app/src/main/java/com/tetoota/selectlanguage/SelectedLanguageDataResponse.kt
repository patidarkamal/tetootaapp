package com.tetoota.selectlanguage
import com.google.gson.annotations.SerializedName
/**
 * Created by charchit.kasliwal on 13-10-2017.
 */
data class SelectedLanguageDataResponse(@SerializedName("langCode") var langCode: String,
                         @SerializedName("labelkey") var labelkey: String,
                         @SerializedName("labelVal") var labelVal: String,
                         @SerializedName("updatedDate") var updatedDate: String)

