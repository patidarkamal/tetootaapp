package com.tetoota.selectlanguage
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tetoota.R
import kotlinx.android.synthetic.main.selectlanguage_row.view.*
import org.jetbrains.anko.onClick
import java.util.*

/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
class SelectLanguageAdapter (var mSelectLanguageList : ArrayList<LanguageDataResponse>
       = ArrayList<LanguageDataResponse>(),  var mArrayList : ArrayList<LanguageDataResponse>
       = ArrayList<LanguageDataResponse>(), val iAdapterClickListener : IAdapterClickListener)
        : RecyclerView.Adapter<SelectLanguageAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.selectlanguage_row, parent, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, p1: Int) {
        viewHolder?.bindHomeData(viewHolder, p1, mSelectLanguageList[p1],iAdapterClickListener)

    }

    fun setElements(mSelectLanguageList: ArrayList<LanguageDataResponse>) {
        this.mArrayList = mSelectLanguageList
        this.mSelectLanguageList = mSelectLanguageList
    }
    private fun getLastPosition() = if (mSelectLanguageList.lastIndex == -1) 0 else mSelectLanguageList.lastIndex

    override fun getItemCount(): Int {
        return mSelectLanguageList.size
    }

    interface IAdapterClickListener {
        fun cellItemClick(mString: String, cellRow: Any): Unit
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_language_text = view.tv_language_text!!
        val rl_maincontent = view.rl_maincontent!!
        fun bindHomeData(viewHolder: ViewHolder?, p1: Int, mLanguageData: LanguageDataResponse, iAdapterClickListener: IAdapterClickListener) {
            tv_language_text.text = mLanguageData.lang_name
            rl_maincontent.onClick {
                iAdapterClickListener.cellItemClick(mLanguageData.lang_name, mLanguageData)
            }

        }

    }

}