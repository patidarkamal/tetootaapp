package com.tetoota.selectlanguage
/**
 * Created by charchit.kasliwal on 13-10-2017.
 */
open class SelectedLanguageResponse {
    open var meta: SelectedLanguageMeta? = null
     var data : List<SelectedLanguageDataResponse>? = null
}