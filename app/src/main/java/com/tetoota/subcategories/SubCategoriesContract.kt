package com.tetoota.subcategories

import android.app.Activity

/**
 * Created by jitendra.nandiya on 13-11-2017.
 */
class SubCategoriesContract {
    interface View {
        fun onSubCategoriesSuccessResult(mCategoriesList: List<DataItem>, message: String)
        fun onSubCatergoriesFailureResult(message: String)
    }

    internal interface Presenter {
        fun getSubCategoriesData(mSubCategoryId : Int, mActivity : Activity)
    }

    interface CategoriesApiListener {
        fun onCategoriesApiSuccess(mDataList: List<DataItem?>?, message: String)
        fun onCategoriesApiFailure(message: String)
    }
}