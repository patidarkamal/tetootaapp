package com.tetoota.subcategories

import android.os.Parcel
import android.os.Parcelable

data class DataItem(val sub_category_image: String? = null,
                    val category_id: Int? = null,
                    val sub_category_id: Int? = null,
                    val sub_category_name: String? = null,
                    val language: String? = null, var isSelected: Boolean = false) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString()) { }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(sub_category_image)
        parcel.writeValue(category_id)
        parcel.writeValue(sub_category_id)
        parcel.writeString(sub_category_name)
        parcel.writeString(language)
        parcel.writeInt((if (isSelected) 1 else 0))
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DataItem> {
        override fun createFromParcel(parcel: Parcel): DataItem {
            return DataItem(parcel)
        }

        override fun newArray(size: Int): Array<DataItem?> {
            return arrayOfNulls(size)
        }
    }
}
