package com.tetoota.subcategories

import android.app.Activity
import com.tetoota.TetootaApplication
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jitendra.nandiya on 13-11-2017.
 */
class SubCategoriesInteractor {
    var mSubCategoriesApiListener: SubCategoriesContract.CategoriesApiListener
    val API_SUCCESS_VALUE = 101
    val API_FAILURE_VALUE = 202

    constructor(mSubCategoriesApiListener: SubCategoriesContract.CategoriesApiListener) {
        this.mSubCategoriesApiListener = mSubCategoriesApiListener
    }

    fun getAllCategoriesData(mCategoryId: Int , mActivity : Activity) : Unit{
        var call: Call<SubCategoriesResponse> = TetootaApplication.getHeader()
                .getSubCategoriesList(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),mCategoryId)
        call.enqueue(object : Callback<SubCategoriesResponse> {
            override fun onFailure(call: Call<SubCategoriesResponse>?, t: Throwable?) {
                println("Review API RESPONSE Failure ${t?.message.toString()}")
                mSubCategoriesApiListener.onCategoriesApiFailure(t?.message.toString())
            }

            override fun onResponse(call: Call<SubCategoriesResponse>?, response: Response<SubCategoriesResponse>?) {
                var mCategoriesListData: SubCategoriesResponse? = response?.body()
                println("UserVisibilityResponse Code ${response?.code()}")
                if(response?.code() == 200){
                    if(mCategoriesListData?.data!!.size > 0){
                        mSubCategoriesApiListener.onCategoriesApiSuccess(mCategoriesListData.data,"success")
                    }else{
                        mSubCategoriesApiListener.onCategoriesApiFailure(response.body()?.meta?.message.toString())
                    }
                }else{
                    mSubCategoriesApiListener.onCategoriesApiFailure(response?.body()?.meta?.message.toString())
                }
            }
        })
    }
}