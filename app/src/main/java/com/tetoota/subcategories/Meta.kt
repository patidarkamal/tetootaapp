package com.tetoota.subcategories

data class Meta(
	val code: Int? = null,
	val message: String? = null,
	val status: Boolean? = null
)
