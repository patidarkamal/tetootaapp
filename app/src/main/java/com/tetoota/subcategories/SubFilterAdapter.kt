package com.android.bizdak.categoery.adapter
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.android.bizdak.categoery.model.SubCategoryModel
import com.tetoota.R
import com.tetoota.subcategories.DataItem

/**
 * Created by vaibhav.malviya on 06-07-2017.
 */
class SubFilterAdapter(var context: Context, var subCategoryList: MutableList<DataItem>) : RecyclerView.Adapter<SubFilterAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.subcategorylist_item_row, parent, false)

        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return subCategoryList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var subCategoryModel: DataItem = subCategoryList[position]
        holder.filterText.text = subCategoryModel.sub_category_name
        if (subCategoryModel.isSelected) {
            holder.arrowImage.setImageResource(R.drawable.iv_chk_box_large)
        } else {
            holder.arrowImage.setImageResource(R.drawable.iv_un_box_large)
        }
        holder.filterLayout.setOnClickListener {
            if(subCategoryModel.isSelected){
                holder.arrowImage.setImageResource(R.drawable.iv_un_box_large)
                subCategoryModel.isSelected = false
            }else{
                holder.arrowImage.setImageResource(R.drawable.iv_chk_box_large)
                subCategoryModel.isSelected = true
            }
//            listener.onFilterSelected(position, categoryModel)
            subCategoryList.set(position, subCategoryModel)

        }
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var arrowImage: ImageView = view.findViewById<ImageView>(R.id.arrowForward)
        var filterText: TextView = view.findViewById<TextView>(R.id.filterText)
        var filterLayout: RelativeLayout = view.findViewById<RelativeLayout>(R.id.filterLayout)
        var clickLayout: LinearLayout = view.findViewById<LinearLayout>(R.id.clickLayout)
    }

    interface SubFilterListener {
        fun onFilterSelected(position: Int, categoryName: SubCategoryModel)
    }

    fun ImageView.loadUrl(url: String, view: ImageView) {
        if (url != null && url.length > 0) {

        }
    }
}