package com.tetoota.appintro
import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import com.squareup.picasso.Picasso
import com.tetoota.R

/**
 * Created by charchit.kasliwal on 09-06-2017.
 */
class ProductDetailSliderAdapter(val mContext: Context,
                                 var mDashboardSlider : ArrayList<String> = ArrayList<String>())
    : PagerAdapter(){
    /**
     *
     */
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as RelativeLayout
    }

    fun setElements(mDashboardSlider: ArrayList<String>){
        this.mDashboardSlider = mDashboardSlider
    }
    /**
     *
     */
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as  RelativeLayout)
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }
    /**
     *
     */
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(mContext).inflate(R.layout.dashboard_slider_row, container, false)
        val imageView = itemView.findViewById<ImageView>(R.id.img_pager_item)

      //  imageView.loadUrl(mDashboardSlider[position], imageView,itemView.context)

        if (mDashboardSlider[position].isEmpty()) {
            imageView.setImageResource(R.drawable.queuelist_place_holder);
        } else{

            Picasso.get().load(mDashboardSlider[position]).into(imageView)
        }

        container?.addView(itemView)
        return itemView
    }

/*
    fun ImageView.loadUrl(url: String,imageView : ImageView,mContext: Context) {
        Glide.with(mContext)
                .load(Utils.getUrl(mContext, url!!))
                .centerCrop()
                .placeholder(R.drawable.queuelist_place_holder)
                .error(R.drawable.queuelist_place_holder)
                .into(imageView)
    }
*/

    override fun getCount(): Int {
        return mDashboardSlider.size
    }
}