package com.tetoota.pointssummary

data class PointSummaryDataResponse(
	val tetoota_points: Int? = null,
	val point_transaction_date: String? = null,
	val post_id: Int? = null,
	val sender_userid: Int? = null,
	val description: String? = null,
	val id: Int? = null,
	val reciver_userid: Int? = null,
	val title: String? = null,
	val fullname: String? = null,
	val history_type: String? = null
)
