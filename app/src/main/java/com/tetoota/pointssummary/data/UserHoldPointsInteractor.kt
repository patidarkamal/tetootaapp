package com.tetoota.pointssummary.data

import android.app.Activity
import com.tetoota.TetootaApplication
import com.tetoota.pointssummary.di.MainPointSummaryContract
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by abhinav.maurya on 01-02-2018.
 */
class UserHoldPointsInteractor {

    var call: Call<UserHoldPointsResponse>? = null
    var mPointsSummaryListener: MainPointSummaryContract.MainPointsSummaryResult

    constructor(mPointsSummaryListener: MainPointSummaryContract.MainPointsSummaryResult) {
        this.mPointsSummaryListener = mPointsSummaryListener
    }

    fun getUserHoldPoints(mActivity: Activity, user_id: String) {
        call = TetootaApplication.getHeader()
                .getUserHoldPoints(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), user_id!!)
        call!!.enqueue(object : Callback<UserHoldPointsResponse> {
            override fun onResponse(call: Call<UserHoldPointsResponse>?,
                                    response: Response<UserHoldPointsResponse>?) {
                var mDashboardSliderData: UserHoldPointsResponse? = response?.body()
                if (response?.code() == 200) {
                    if (mDashboardSliderData != null && mDashboardSliderData.data != null) {
                        mPointsSummaryListener.onUserHoldPointsAPiSuccess("success", mDashboardSliderData.data!!)
                    } else {
                        mPointsSummaryListener.onUserHoldPointsAPiFailure("failure", false)
                    }
                } else {
                }
            }

            override fun onFailure(call: Call<UserHoldPointsResponse>?, t: Throwable?) {
                mPointsSummaryListener.onUserHoldPointsAPiFailure(t?.message.toString(), false)
            }
        })
    }

}