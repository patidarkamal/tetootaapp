package com.tetoota.pointssummary.data

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("user_hold_points")
	val userHoldPoints: Int? = null
)