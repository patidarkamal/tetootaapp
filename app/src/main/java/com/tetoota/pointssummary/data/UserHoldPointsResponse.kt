package com.tetoota.pointssummary.data

import com.google.gson.annotations.SerializedName
import com.tetoota.network.errorModel.Meta

data class UserHoldPointsResponse(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("meta")
	val meta: Meta? = null
)