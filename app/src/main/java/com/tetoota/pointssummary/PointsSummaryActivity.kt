package com.tetoota.pointssummary

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.login.LoginDataResponse
import com.tetoota.pointssummary.data.Data
import com.tetoota.pointssummary.di.MainPointSummaryContract
import com.tetoota.pointssummary.di.MainPointSummaryPresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_points_summary.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick

class PointsSummaryActivity : BaseActivity(), MainPointSummaryContract.View {

    val mPointsSummaryPresenter : MainPointSummaryPresenter by lazy {
        MainPointSummaryPresenter(this@PointsSummaryActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        this.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_points_summary)
        initViews()

    }

    private fun initViews(): Unit {
//        async { rl_totalpoints.layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, heightCalculation(0.35f)) }
        iv_close.visibility = View.VISIBLE
        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
        iv_close.onClick { onBackPressed() }
        setupViewPager(view_pager_container)
        tab_layout.setupWithViewPager(view_pager_container)
        setMultiLanguageText()
        if(Utils.haveNetworkConnection(this)){
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
            val personData  = Gson().fromJson(json, LoginDataResponse::class.java)
            mPointsSummaryPresenter.holdPointData(this, personData.user_id!!)
        }else{

        }
    }

    private fun setMultiLanguageText() {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        btn_total_tps_pt.text = personData.tetoota_points
        tv_pts_left_description.text = Utils.getText(this,StringConstant.you_have) + " " + personData.tetoota_points + " " +
                Utils.getText(this,StringConstant.tetoota_pts_left)
        toolbar_title.text = Utils.getText(this,StringConstant.points_summary)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@PointsSummaryActivity)
        finish()
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, PointsSummaryActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(PointsSpentFragment(), Utils.getText(this,StringConstant.point_spent))
        adapter.addFragment(PointsReceivedFragment(), Utils.getText(this,StringConstant.point_received))
        adapter.addFragment(PointsHoldFragment(), Utils.getText(this,StringConstant.point_hold))
        viewPager.adapter = adapter
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private var mFragmentList: MutableList<Fragment> = ArrayList()
        private var mFragmentTitleList: MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

    override fun onUserHoldPointsAPiSuccess(mString: String, holdPointResponse: Data) {
        if(holdPointResponse != null){
            /*val text = Utils.getText(this, StringConstant.history_hold_points
                    + " : " + holdPointResponse.userHoldPoints)
            if(text == null || text.equals("")){
                tv_pts_hold_description.text = Utils.getText(this,StringConstant.history_hold_points)+" " + holdPointResponse.userHoldPoints
            } else{
                tv_pts_hold_description.text = text
            }*/
            val holdPoints = holdPointResponse.userHoldPoints
            if(holdPoints == null || holdPoints.equals("") || holdPoints < 0){
                tv_pts_hold_description.text = Utils.getText(this,StringConstant.history_hold_points)+" : "+ 0
            } else{
                tv_pts_hold_description.text = Utils.getText(this,StringConstant.history_hold_points)+" : " + holdPointResponse.userHoldPoints
            }
        }
    }

    override fun onUserHoldPointsAPiFailure(mString: String, isServerError: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
