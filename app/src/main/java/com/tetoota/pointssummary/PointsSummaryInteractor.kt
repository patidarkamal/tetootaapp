package com.tetoota.pointssummary

import android.app.Activity
import com.tetoota.TetootaApplication
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
/**
 * Created by charchit.kasliwal on 20-06-2017.
 */
class PointsSummaryInteractor {
    var call : Call<PointsSummaryResponse>? = null
    var mPointsSummaryListener : PointsSummaryContract.PointsSummaryResult

    constructor(mPointsSummaryListener: PointsSummaryContract.PointsSummaryResult) {
        this.mPointsSummaryListener = mPointsSummaryListener
    }

    fun pointsSpentList(mActivity: Activity, history_type : String?, user_id : String){
        call = TetootaApplication.getHeader()
                .pointsHistory(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity),
                        history_type!!, user_id)
        call!!.enqueue(object : Callback<PointsSummaryResponse> {
            override fun onResponse(call: Call<PointsSummaryResponse>?,
                                    response: Response<PointsSummaryResponse>?) {
                var mDashboardSliderData : PointsSummaryResponse? = response?.body()
                if(response?.code() == 200){
                    if(response.body()?.data?.size!! > 0){
                        if (mDashboardSliderData != null && mDashboardSliderData.data?.size!! > 0) {
                            mPointsSummaryListener.onPointSpendSuccess("success", mDashboardSliderData.data!!)
                        }else{
                            mPointsSummaryListener.onPointSpentFailure("failure", false)
                        }
                    }else{
                        mPointsSummaryListener.onPointSpentFailure("failure", false)
                    }
                }else{
                }
            }
            override fun onFailure(call: Call<PointsSummaryResponse>?, t: Throwable?) {
                mPointsSummaryListener.onPointSpentFailure(t?.message.toString(), false)
            }
        })
    }


}