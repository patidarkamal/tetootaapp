package com.tetoota.filter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.android.bizdak.categoery.adapter.FilterAdapter
import com.android.bizdak.categoery.model.CategoryModel
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.categories.CategoriesDataResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_filter_category.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.json.JSONArray
class FilterCategoryActivity : BaseActivity(), FilterAdapter.FilterListener {
    private var selectedfilterPoition: Int = -1
    var mCategoryList : ArrayList<CategoriesDataResponse> = ArrayList()
    lateinit var selectedfilter: CategoryModel
    var PICK_FILTER_REQUEST: Int = 1

    override fun onFilterSelected(position: Int, categoryName: CategoriesDataResponse) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        if(!mSelectedcategorylist.contains(categoryName)){
            mSelectedcategorylist.add(categoryName)
        }
        personData.categories?.clear()
        personData.categories = mSelectedcategorylist as ArrayList<CategoriesDataResponse?>
        val json1 = Gson().toJson(personData)
        Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json1, this@FilterCategoryActivity)

        /*val intent = SubFilterActivity.newMainIntent(this@FilterCategoryActivity)
        intent?.putParcelableArrayListExtra("categorylist", mCategoryList)
        intent?.putParcelableArrayListExtra("mSelectedcategorylist", personData.categories)
        ActivityStack.getInstance(this@FilterCategoryActivity)
        startActivity(intent)*/
    }

    // var filterList: MutableList<CategoryModel> = mutableListOf()
    lateinit var listener: FilterAdapter.FilterListener
    lateinit var filterAdapter: FilterAdapter
    var categoryList: MutableList<CategoriesDataResponse> = mutableListOf()
    var mSelectedcategorylist :  ArrayList<CategoriesDataResponse> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_category)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar_title.text = "Category"
        categoryList =   intent.getParcelableArrayListExtra<CategoriesDataResponse>("categorylist")
        mSelectedcategorylist =  intent.getParcelableArrayListExtra<CategoriesDataResponse>("mSelectedcategorylist")
        mCategoryList = (categoryList as ArrayList<CategoriesDataResponse>?)!!
        filterAdapter = FilterAdapter(this, categoryList, this)
        val mLayoutManager = LinearLayoutManager(this)
        recycler_view.layoutManager = mLayoutManager
        recycler_view.itemAnimator = DefaultItemAnimator()
        recycler_view.adapter = filterAdapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Check which request we're responding to
        if (requestCode == PICK_FILTER_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
               /* var bundal: Bundle? = pushNotificationDataResponse?.extras
                var subFilter: MutableList<SubCategoryModel> = bundal?.getParcelableArrayList("FILTER")!!
                categoryList[selectedfilterPoition].childCategories = subFilter
                filterAdapter.notifyDataSetChanged()
                for (item in subFilter) {
                    var st: String = "$" + item.isMasterSubCategorySelected
                    Log.d("List:-", st)

                }*/
                // The user picked a contact.
                // The Intent's pushNotificationDataResponse Uri identifies which contact was selected.

                // Do something with the contact here (bigger example below)
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@FilterCategoryActivity)
        finish()
    }

    fun generateJson(){
        var jsonArray  = JSONArray()

    }
    companion object{
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, FilterCategoryActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.isVisible = true
        return super.onCreateOptionsMenu(menu)

    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }
}
