package com.tetoota.categories

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tetoota.R
import kotlinx.android.synthetic.main.message_item_child.view.*
import java.util.*
/**
 * Created by charchit.kasliwal on 03-07-2017.
 */
class FilteredAdapter (var mCategoriesDataResponse : ArrayList<CategoriesDataResponse>
                                 = ArrayList<CategoriesDataResponse>(),  val iAdapterClickListener : ISelectedAdapterClickListener)
    : RecyclerView.Adapter<FilteredAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.message_item_child, parent, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, p1: Int) {
        viewHolder.bindHomeData(viewHolder, p1, mCategoriesDataResponse[p1],iAdapterClickListener)
    }

    fun setElements(mSelectLanguageList: ArrayList<CategoriesDataResponse>) {
        this.mCategoriesDataResponse = mSelectLanguageList
    }
    private fun getLastPosition() = if (mCategoriesDataResponse.lastIndex == -1) 0 else mCategoriesDataResponse.lastIndex

    override fun getItemCount(): Int {
        return mCategoriesDataResponse.size
    }
    fun setItem(pos: Int?) {
        if (pos != null) {
            mCategoriesDataResponse.removeAt(pos)
            notifyItemRemoved(pos)
            notifyItemRangeRemoved(pos, itemCount)
        }
    }


    interface ISelectedAdapterClickListener {
        fun cellSelectedItemClick(mString: Int, cellRow: Any): Unit
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_categoryname = view.tv_categoryname!!
        val iv_close  = view.iv_close!!
        val ll_mainlayout_layout = view.ll_mainlayout_layout!!
        fun bindHomeData(viewHolder: ViewHolder?, p1: Int, mCategoryData: CategoriesDataResponse,
                         iAdapterClickListener: ISelectedAdapterClickListener) {
            tv_categoryname.text = mCategoryData.category_name
            ll_mainlayout_layout.setOnClickListener(View.OnClickListener {
                iAdapterClickListener.cellSelectedItemClick(p1, mCategoryData)
            })
            /*  iv_close.setOnClickListener(View.OnClickListener {

              })*/
        }

    }

}