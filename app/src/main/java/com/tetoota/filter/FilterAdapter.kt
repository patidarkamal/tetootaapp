package com.android.bizdak.categoery.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.tetoota.R
import com.tetoota.categories.CategoriesDataResponse

/**
 * Created by vaibhav.malviya on 05-07-2017.
 */
class FilterAdapter(var context: Context, var categoryList: MutableList<CategoriesDataResponse>, var listener: FilterListener) : RecyclerView.Adapter<FilterAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.filter_item_row, parent, false)

        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var categoryModel: CategoriesDataResponse = categoryList[position]
        holder.filterName.text = categoryModel.category_name
        holder.clickLayout.setOnClickListener {
            listener.onFilterSelected(position, categoryModel)
        }
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var arrowImage: ImageView = view.findViewById<ImageView>(R.id.arrowForward)
        var subFilter: TextView = view.findViewById<TextView>(R.id.selectionSubFilter)
        var filterName: TextView = view.findViewById<TextView>(R.id.filterText)
        var filterImage: ImageView = view.findViewById<ImageView>(R.id.cate_image)
        //  var filterCheckBox: CheckBox = view.findViewById(R.id.checkBox) as CheckBox
        var filterLayout: RelativeLayout = view.findViewById<RelativeLayout>(R.id.filterLayout)
        var clickLayout: LinearLayout = view.findViewById<LinearLayout>(R.id.clickLayout)
    }

    interface FilterListener {
        fun onFilterSelected(position: Int, categoryName: CategoriesDataResponse)
    }

    fun ImageView.loadUrl(url: String, view: ImageView) {
        if (url != null && url.length > 0) {
        }
    }
}