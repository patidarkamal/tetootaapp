package com.tetoota.splash

import android.app.Activity

/**
 * Created by charchit.kasliwal on 01-06-2017.
 */
class SplashContract {
    /**
     * the view success
     */
    interface View {
        fun onMainActivitySuccess()

        fun onLoginActivitySuccess()
    }
    internal interface Interactor {
        fun appSession()
    }
    internal interface Presenter {
        fun checkAppSession(activity: Activity)
    }
    interface onSplashListener {
        fun onMainActivitySuccess(message: String)

        fun onLoginActivitySuccess(message: String)
    }
}