package com.tetoota.splash
import android.app.Activity

/**
 * Created by charchit.kasliwal on 01-06-2017.
 */
class SplashPresenter : SplashContract.Presenter,SplashContract.onSplashListener{
    var mSplashContract : SplashContract.View
    val mSplashInteractor = SplashInteractor(this)
    /**
     * Secondary Constructor
     */
    constructor(mSplashContract: SplashContract.View) {
        this.mSplashContract = mSplashContract
    }
    override fun checkAppSession(activity: Activity) {
        mSplashInteractor.appSession(activity)
    }
    override fun onMainActivitySuccess(message: String) {
     mSplashContract.onMainActivitySuccess()
    }
    override fun onLoginActivitySuccess(message: String) {
     mSplashContract.onLoginActivitySuccess()
    }
}