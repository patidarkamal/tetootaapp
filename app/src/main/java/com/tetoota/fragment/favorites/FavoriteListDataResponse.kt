package com.tetoota.service_product

data class FavoriteListDataResponse(
		val id : String? = null,
		val user_id: String? = null,
		val categories_ids: String? = null,
		val title: String? = null,
		val description: String? = null,
		val set_quote: String? = null,
		val qualification: String? = null,
		val availability: String? = null,
		val trading_preference: String? = null,
		val set_tetoota_points: String? = null,
		val latitude: String? = null,
		val longitude: String? = null,
		val post_image: String? = null,
		val virtual_service: String? = null,
		val share_service_platform: String? = null,
		val creation_date: String? = null,
		val post_type: String? = null)

