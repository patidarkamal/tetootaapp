package com.tetoota.fragment.favorites

import android.app.Activity
import com.tetoota.fragment.dashboard.ServicesDataResponse

/**
 * Created by charchit.kasliwal on 12-06-2017.
 */
class FavoriteContract {

    interface View{
          fun onFavoriteListSuccess(mString: String, mFavoriteList: ArrayList<ServicesDataResponse>) : Unit
          fun onFavoriteFailureResponse(mString: String,isServerError: Boolean) : Unit
    }

    interface Presenter{
        fun getUserFavoriteList(mActivity : Activity,pagination : Int?)
    }

    interface FavoriteApiResult{
          fun onFavoriteApiSuccess(mString : String, mFavoriteList : ArrayList<ServicesDataResponse>) : Unit
          fun onFavoriteApiFailure(mString : String, isServerError: Boolean) : Unit
    }
}