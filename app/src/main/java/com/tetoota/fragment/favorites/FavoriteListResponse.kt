package com.tetoota.service_product
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.network.errorModel.Meta
data class FavoriteListResponse(
		val data: List<ServicesDataResponse?>? = null,
		val meta: Meta? = null
)
