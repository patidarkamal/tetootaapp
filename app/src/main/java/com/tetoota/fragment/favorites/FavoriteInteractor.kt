package com.tetoota.fragment.favorites

import android.app.Activity
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.service_product.UserServiceResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by charchit.kasliwal on 12-06-2017.
 */
class FavoriteInteractor {
    var call : Call<UserServiceResponse>? = null
    var mFavoriteListener : FavoriteContract.FavoriteApiResult

    constructor(mFavoriteListener: FavoriteContract.FavoriteApiResult) {
        this.mFavoriteListener = mFavoriteListener
    }

    fun getFavoriteList(mActivity: Activity,pagination : Int?){
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData  = Gson().fromJson(json, LoginDataResponse::class.java)
        call = TetootaApplication.getHeader()
                .getFavoriteList(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity),
                        personData.user_id!!, pagination!!)
        call!!.enqueue(object : Callback<UserServiceResponse> {
            override fun onResponse(call: Call<UserServiceResponse>?,
                                    response: Response<UserServiceResponse>?) {
                var mDashboardSliderData : UserServiceResponse? = response?.body()
                if(response?.code() == 200){
                    if(response.body()?.data?.size!! > 0){
                        if (mDashboardSliderData != null) {
                            mFavoriteListener.onFavoriteApiSuccess("success", mDashboardSliderData.data as ArrayList<ServicesDataResponse>)
                        }
                    }else{
                        mFavoriteListener.onFavoriteApiFailure(response.body()?.meta?.message.toString(), false)
                    }
                }else{
                    mFavoriteListener.onFavoriteApiFailure(response?.body()?.meta?.message.toString(), true)
                }
            }
            override fun onFailure(call: Call<UserServiceResponse>?, t: Throwable?) {
                mFavoriteListener.onFavoriteApiFailure(t?.message.toString(), true)
            }
        })
    }
}