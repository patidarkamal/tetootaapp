package com.tetoota.fragment.nearby

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("category_name")
	val categoryName: String? = null,

	@field:SerializedName("user_last_name")
	val userLastName: String? = null,

	@field:SerializedName("all_images")
	val allImages: Any? = null,

	@field:SerializedName("availability")
	val availability: String? = null,

	@field:SerializedName("trading_preference")
	val tradingPreference: Int? = null,

	@field:SerializedName("creation_date")
	val creationDate: String? = null,

	@field:SerializedName("is_favourite")
	val isFavourite: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("set_quote")
	val setQuote: Int? = null,

	@field:SerializedName("content")
	val content: String? = null,

	@field:SerializedName("tetoota_points")
	val tetootaPoints: Int? = null,

	@field:SerializedName("qualification")
	val qualification: String? = null,

	@field:SerializedName("user_first_name")
	val userFirstName: String? = null,

	@field:SerializedName("profile_image")
	val profileImage: String? = null,

	@field:SerializedName("reviews")
	val reviews: Int? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("Long")
	val long: Double? = null,

	@field:SerializedName("post_type")
	val postType: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("category_ids")
	val categoryIds: Int? = null,

	@field:SerializedName("post_address")
	val postAddress: String? = null,

	@field:SerializedName("Lat")
	val lat: Double? = null,

	@field:SerializedName("virtual_service")
	val virtualService: String? = null
)