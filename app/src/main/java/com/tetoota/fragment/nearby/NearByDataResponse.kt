package com.tetoota.fragment.nearby

import com.google.gson.annotations.SerializedName

data class NearByDataResponse(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("meta")
	val meta: Meta? = null
)