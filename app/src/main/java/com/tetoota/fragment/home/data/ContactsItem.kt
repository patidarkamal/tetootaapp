package com.tetoota.fragment.home.data

import com.google.gson.annotations.SerializedName

class ContactsItem(

	@field:SerializedName("number")
	var number: String? = null,

	@field:SerializedName("name")
	var name: String? = null


)