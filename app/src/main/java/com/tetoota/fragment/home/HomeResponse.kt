package com.tetoota.fragment.dashboard

import android.os.Parcel
import android.os.Parcelable
import com.tetoota.network.errorModel.Meta

data class HomeResponse(
        val data: ArrayList<HomeDataResponse?>? = null,
        val meta: Meta? = null) : Parcelable {

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HomeResponse> = object : Parcelable.Creator<HomeResponse> {
            override fun createFromParcel(source: Parcel): HomeResponse = HomeResponse(source)
            override fun newArray(size: Int): Array<HomeResponse?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(
            source.createTypedArrayList(HomeDataResponse.CREATOR)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeTypedList(data)
    }
}
