package com.tetoota.fragment.home.data

import com.google.gson.annotations.SerializedName

data class Meta(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)