package com.tetoota.fragment.home.data

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("contact")
	val contact: Long? = null,

	@field:SerializedName("name")
	val name: String? = null
)