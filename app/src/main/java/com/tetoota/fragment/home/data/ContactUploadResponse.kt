package com.tetoota.fragment.home.data

import com.google.gson.annotations.SerializedName

data class ContactUploadResponse(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("meta")
	val meta: Meta? = null
)