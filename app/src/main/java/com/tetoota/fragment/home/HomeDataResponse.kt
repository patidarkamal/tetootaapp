package com.tetoota.fragment.dashboard

import android.os.Parcel
import android.os.Parcelable

data class HomeDataResponse(
        val trending: List<ServicesDataResponse?>? = null,
        val services: List<ServicesDataResponse?>? = null,
        val products: List<ServicesDataResponse?>? = null) : Parcelable {
    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HomeDataResponse> = object : Parcelable.Creator<HomeDataResponse> {
            override fun createFromParcel(source: Parcel): HomeDataResponse = HomeDataResponse(source)
            override fun newArray(size: Int): Array<HomeDataResponse?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(
            source.createTypedArrayList(ServicesDataResponse.CREATOR),
            source.createTypedArrayList(ServicesDataResponse.CREATOR),
            source.createTypedArrayList(ServicesDataResponse.CREATOR)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeTypedList(trending)
        dest.writeTypedList(services)
        dest.writeTypedList(products)
    }
}
