package com.tetoota.homescreen

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.util.Log
import com.tetoota.fragment.home.HomeFragment
import com.tetoota.fragment.nearby.NearByFragment
import com.tetoota.service_product.ProductFragment
import com.tetoota.service_product.ServiceFragment
import com.tetoota.service_product.WishlistFragment
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils

/**
 * Created by charchit.kasliwal on 28-08-2017.
 */
class DashboardPagerStateAdapter(var mContext: Context, fm: FragmentManager, var NumOfTabs: Int,
                                 var iChildClickListener : HomeFragment.OnChildFragmentInteractionListener) : FragmentStatePagerAdapter(fm) {
    var fragmentHome : HomeFragment? = null
    var fragmentService : ServiceFragment? = null
    var fragmentProduct : ProductFragment? = null
    var fragmentWishlist : WishlistFragment? = null
    var fragmentNearBy : NearByFragment? = null
    override fun getCount(): Int {
        return NumOfTabs
    }

    override fun getItem(position: Int): Fragment? {
        println("Tab Position Adapter $position")
        when (position) {
            0 -> {
                fragmentHome = HomeFragment()
                fragmentHome!!.setmListener(iChildClickListener)
//                fragment.setFilterListener(iFilterListener)
                val tab1 = fragmentHome
                return tab1
            }
            1 -> {
                fragmentService = ServiceFragment.newInstance("haveNotUserId","no", true)
                return fragmentService
            }
            2 -> {
                fragmentProduct = ProductFragment.newInstance("haveNotUserId","no", true)
                return fragmentProduct
            }
            3 -> {
//                fragmentWishlist = WishlistFragment()
                fragmentWishlist = WishlistFragment.newInstance("haveNotUserId","no", true)
                return fragmentWishlist
            }
            4 -> {
                fragmentNearBy = NearByFragment()
                return fragmentNearBy
            }
            else -> return null
        }
    }


    override fun getPageTitle(position: Int): CharSequence? {
        var title: String? = null
        if (position == 0) {
            title = Utils.getText(mContext, StringConstant.home)
            // title = "Home1";
        } else if (position == 1) {
            title = Utils.getText(mContext, StringConstant.services)
            // title = "Services1";

        } else if (position == 2) {
            title = Utils.getText(mContext, StringConstant.product)
            //title = "Products1";

        } else if (position == 3) {
            title = Utils.getText(mContext, StringConstant.wishlist)
            // title = "Wishlist1";

        } else if (position == 4) {
            title = Utils.getText(mContext, StringConstant.near_by)
            //title = "Nearby1";

        }
        return title
    }


    var mFilterListener: OnFilterAppliedListener? = null

    interface OnFilterAppliedListener {
        fun filterData(mTopRated: String, mCity: String)
    }

    fun updateFreagment(){
        this.notifyDataSetChanged()
    }
}