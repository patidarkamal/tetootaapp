package com.tetoota.fragment.dashboard
data class FavoriteDataResponse(
	val code: Int? = null,
	val message: String? = null,
	val status: Boolean? = null
)
