package com.tetoota.fragment.dashboard

import com.tetoota.network.errorModel.Meta

data class DashSliderResponse(
		val data: ArrayList<DashboardSliderDataResponse>? = null,
		val meta: Meta? = null
)
