package com.tetoota.fragment.dashboard

data class FavoriteResponse(
	val data: List<Any?>? = null,
	val meta: FavoriteDataResponse? = null
)
