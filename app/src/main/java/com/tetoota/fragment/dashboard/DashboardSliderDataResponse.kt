package com.tetoota.fragment.dashboard

data class DashboardSliderDataResponse(
	val slides_image: String? = null,
	val slides_image_thumb : String? = null,
	val slides_update_date: String? = null,
	val language: String? = null
)
