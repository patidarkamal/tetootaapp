package com.tetoota.fragment.inbox
import android.app.Activity
import com.tetoota.fragment.inbox.pojo.ChatHistoryDataResponse
import com.tetoota.fragment.inbox.pojo.OneToOneChatDataResponse

/**
 * Created by charchit.kasliwal on 09-08-2017.
 */
class InboxPresenter : InboxContract.Presenter,InboxContract.InboxApiListener{
    var mInboxContract: InboxContract.View

    private val mInboxInteractor : InboxInteractor by lazy {
        InboxInteractor(this)
    }

    constructor(mInboxContract: InboxContract.View) {
        this.mInboxContract = mInboxContract
    }

    override fun getProposalData(mActivity: Activity, userId: String) {
      mInboxInteractor.getAllProposalApiCalling(mActivity,userId)
    }

    override fun getChatHistoryData(mActivity: Activity, userId: String) {
     mInboxInteractor.getChatHistoryData(mActivity,userId)
    }

    override fun getOneToOneChatData(mActivity: Activity,msg_send_user_id: String,
                                     msg_rec_user_id:  String,user_id:String, post_id: String){
        mInboxInteractor.oneToOneChatRecord(mActivity, msg_send_user_id,msg_rec_user_id,user_id,post_id)
    }

    override fun sendMesg(mActivity: Activity, msg_send_user_id: String?,
                          msg_rec_user_id:  String, post_id: String, chat_mesg: String,
                          proposal_id: String) {
        if (msg_send_user_id != null) {
            mInboxInteractor.sendChatMesg(mActivity,msg_send_user_id,msg_rec_user_id,post_id,chat_mesg,proposal_id)
        }
    }


    override fun onApiSuccess(mProposalMesgData : ArrayList<ProposalMessageData>,message: String) {
        mInboxContract.ongetAllProposalSuccess(mProposalMesgData,message)
    }

    override fun onApiFailure(message: String) {
        mInboxContract.ongetAllProposalFailure(message)
    }

    override fun chatHistorySuccess(mChatHistoryResponse: ArrayList<ChatHistoryDataResponse?>?) {
        super.chatHistorySuccess(mChatHistoryResponse)
        mInboxContract.onChatHistorySuccess(mChatHistoryResponse as ArrayList<ChatHistoryDataResponse>)
    }

    override fun chatHistoryFailure(message: String) {
        super.chatHistoryFailure(message)
       mInboxContract.onChatHistoryFailure(message)
    }


    override fun chatSendMesgSuccess(message: String) {
        super.chatSendMesgSuccess(message)
        mInboxContract.onChatMesgSuccess(message)
    }

    override fun chatSendMesgFailure(message: String) {
        super.chatSendMesgFailure(message)
        mInboxContract.onChatMesgFailure(message)
    }


    override fun oneToOneSuccess(message: String, mChatList: ArrayList<OneToOneChatDataResponse?>?) {
        super.oneToOneSuccess(message, mChatList)
        mInboxContract.onOneToOneSuccess(message,mChatList as ArrayList<OneToOneChatDataResponse>)
    }

    override fun oneToOneFailure(message: String) {
        mInboxContract.onOneToOneFailure(message)
    }
}