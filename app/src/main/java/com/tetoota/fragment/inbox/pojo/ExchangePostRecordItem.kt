package com.tetoota.fragment.inbox

import android.os.Parcel
import android.os.Parcelable

data class ExchangePostRecordItem(
        val post_image: String? = null,
        val latitude: String? = null,
        val description: String? = null,
        val availability: String? = null,
        val trading_preference: String? = null,
        val creation_date: String? = null,
        val title: String? = null,
        val set_quote: String? = null,
        val qualification: String? = null,
        val userId: String? = null,
        val share_service_platform: String? = null,
        val post_type: String? = null,
        val id: String? = null,
        val set_tetoota_points: String? = null,
        val categories_ids: String? = null,
        val longitude: String? = null,
        val virtual_service: String? = null
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(post_image)
        writeString(latitude)
        writeString(description)
        writeString(availability)
        writeString(trading_preference)
        writeString(creation_date)
        writeString(title)
        writeString(set_quote)
        writeString(qualification)
        writeString(userId)
        writeString(share_service_platform)
        writeString(post_type)
        writeString(id)
        writeString(set_tetoota_points)
        writeString(categories_ids)
        writeString(longitude)
        writeString(virtual_service)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ExchangePostRecordItem> = object : Parcelable.Creator<ExchangePostRecordItem> {
            override fun createFromParcel(source: Parcel): ExchangePostRecordItem = ExchangePostRecordItem(source)
            override fun newArray(size: Int): Array<ExchangePostRecordItem?> = arrayOfNulls(size)
        }
    }
}

