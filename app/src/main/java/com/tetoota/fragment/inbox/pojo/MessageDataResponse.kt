package com.tetoota.fragment.inbox.pojo

/**
 * Created by charchit.kasliwal on 19-09-2017.
 */
class MessageDataResponse(val messageTo: String? = null,
                          val messageDesc: String? = null,
                          private val mesgUserImg: String? = null) {
}