package com.tetoota.fragment.inbox.pojo
import com.tetoota.network.errorModel.Meta
data class OneToOneChatResponse(
        val data: ArrayList<OneToOneChatDataResponse?>? = null,
        val meta: Meta? = null
)
