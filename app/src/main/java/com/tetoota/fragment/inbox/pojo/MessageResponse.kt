package com.tetoota.fragment.inbox.pojo
import com.tetoota.network.errorModel.Meta

/**
 * Created by charchit.kasliwal on 19-09-2017.
 */
class MessageResponse( val data: List<MessageDataResponse?>? = null,
                       val meta: Meta? = null) {

}