package com.tetoota.fragment.inbox.pojo

data class OneToOneChatDataResponse(
        var msg_recieved_user_id: String? = null,
        var msg_send_user_id: String? = null,
        var post_id: String? = null,
        var proposal_id: String? = null,
        var chatMesgDateTime: String? = null,
        var chatMessageId: String? = null,
        var chat_message: String? = null,
        var conver_id: String? = null,
        var trading_status: String? = null,
        var proposal_status: String? = null,
        var msg_sender_profile_img: String? = null,
        var msg_recieved_profile_img: String? = null,
        var receiver_trading_status: String? = null
)
