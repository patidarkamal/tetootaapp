package com.tetoota.fragment.inbox

import android.app.Activity
import com.tetoota.TetootaApplication
import com.tetoota.fragment.inbox.pojo.ChatHistoryResponse
import com.tetoota.fragment.inbox.pojo.OneToOneChatResponse
import com.tetoota.fragment.inbox.pojo.ProposalApiResponse
import com.tetoota.fragment.inbox.pojo.SendChatMesg
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by charchit.kasliwal on 09-08-2017.
 */
class InboxInteractor {
    var call : Call<ProposalApiResponse>? = null
    var mInboxApiListener : InboxContract.InboxApiListener
    constructor(mInboxApiListener: InboxContract.InboxApiListener) {
        this.mInboxApiListener = mInboxApiListener
    }
    fun getAllProposalApiCalling(mActivity : Activity, mUserId : String){
        call = TetootaApplication.getHeader()
                .getProposalMessageList(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity), mUserId)
        call!!.enqueue(object : Callback<ProposalApiResponse> {
            override fun onResponse(call: Call<ProposalApiResponse>?, response: Response<ProposalApiResponse>?) {
                println("Success")
                var mWalkThroughData : ProposalApiResponse? = response?.body()
                if(response?.code() == 200){
                    if(response.body()?.data != null){
                        if(response.body()?.data?.size!! > 0){
                            mInboxApiListener.onApiSuccess(response.body()!!.data as ArrayList<ProposalMessageData>,"success")
                        }else{
                            mInboxApiListener.onApiFailure("No Records Found")
                        }
                    }
                }else{
                    mInboxApiListener.onApiFailure(response?.body()?.meta?.message.toString())
                }
            }
            override fun onFailure(call: Call<ProposalApiResponse>?, t: Throwable?) {
                mInboxApiListener.onApiFailure(t?.message.toString())
            }
        })
    }

    fun getChatHistoryData(mActivity : Activity, mUserId : String){
        var  call : Call<ChatHistoryResponse> = TetootaApplication.getHeader()
                .getChatHistory(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity),mUserId)
        call.enqueue(object : Callback<ChatHistoryResponse> {
            override fun onResponse(call: Call<ChatHistoryResponse>?, response: Response<ChatHistoryResponse>?) {
                println("Success")
                if(response?.code() == 200){
                    if(response.body()?.data != null){
                        mInboxApiListener.chatHistorySuccess(response.body()!!.data)
                    }else{
                        mInboxApiListener.chatHistoryFailure(response.message())
                    }
                }else{
                    mInboxApiListener.chatHistoryFailure(response?.body()?.meta?.message.toString())
                }
            }
            override fun onFailure(call: Call<ChatHistoryResponse>?, t: Throwable?) {
                mInboxApiListener.chatHistoryFailure(t?.message.toString())
            }
        })
    }


     fun sendChatMesg(mActivity: Activity,msg_send_user_id : String, msg_recieved_user_id : String
    ,post_id : String,chat_message : String, proposal_id : String ){
        var  call : Call<SendChatMesg> = TetootaApplication.getHeader()
                 .sendChatMesg(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                         Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                         Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity), msg_send_user_id!!,
                         msg_recieved_user_id,post_id,chat_message, proposal_id)
         call.enqueue(object : Callback<SendChatMesg> {
             override fun onResponse(call: Call<SendChatMesg>?, response: Response<SendChatMesg>?) {
                 println("Success")
                 if(response?.code() == 200){
                     if(response.body()?.data != null){
                             mInboxApiListener.chatSendMesgSuccess(response.message())
                         }else{
                             mInboxApiListener.chatSendMesgFailure(response.message())
                         }
                 }else{
                     mInboxApiListener.onApiFailure(response?.body()?.meta?.message.toString())
                 }
             }
             override fun onFailure(call: Call<SendChatMesg>?, t: Throwable?) {
                 mInboxApiListener.onApiFailure(t?.message.toString())
             }
         })
    }


    /**
     *
     */
    fun oneToOneChatRecord(mActivity: Activity,msg_send_user_id : String, msg_recieved_user_id : String,
                           user_id : String,post_id : String){
        var  call : Call<OneToOneChatResponse> = TetootaApplication.getHeader()
                .getOneToOneChatRecord(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity),msg_send_user_id, msg_recieved_user_id,user_id, post_id)
        call.enqueue(object : Callback<OneToOneChatResponse> {
            override fun onResponse(call: Call<OneToOneChatResponse>?, response: Response<OneToOneChatResponse>?) {
                println("Success")
                if(response?.code() == 200){
                    if(response.body()?.data != null){
                           mInboxApiListener.oneToOneSuccess("success", response.body()!!.data)
                    }else{
                            mInboxApiListener.oneToOneFailure("failure")
                    }
                }else{
                    mInboxApiListener.oneToOneFailure("failure")
                }
            }
            override fun onFailure(call: Call<OneToOneChatResponse>?, t: Throwable?) {
                mInboxApiListener.oneToOneFailure(t?.message.toString())
            }
        })
    }
}