package com.tetoota.fragment.inbox

import android.app.Activity
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.fragment.inbox.pojo.delete_chat_data_responce.DeleteChatDataResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jitendra.nandiya on 11-12-2017.
 */
class DeleteChatInteractor {
    var call: Call<DeleteChatDataResponse>? = null
    var mDeleteChatListner: DeleteChatContract.DeleteChatApiResult

    constructor(mDeleteChatListner: DeleteChatContract.DeleteChatApiResult) {
        this.mDeleteChatListner = mDeleteChatListner
    }

    fun getDeleteChat(mActivity: Activity, conver_id: Int?, position : Int?) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)

        call = TetootaApplication.getHeader().deleteChatMesg(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),
                conver_id!!,
                personData.user_id!!)

        call!!.enqueue(object : Callback<DeleteChatDataResponse> {
            override fun onResponse(call: Call<DeleteChatDataResponse>?,
                                    response: Response<DeleteChatDataResponse>?) {
                var mDashboardSliderData: DeleteChatDataResponse? = response?.body()
                if (response?.code() == 200) {
                    if (response.body()?.meta?.status!!) {
                        if (mDashboardSliderData != null) {
                            mDeleteChatListner.onDeleteChatApiSuccess(response.body()?.meta?.message.toString(), position!!)
                        }
                    } else {
                        mDeleteChatListner.onDeleteChatApiFailure(response.body()?.meta?.message.toString(), false)
                    }
                } else {
                    mDeleteChatListner.onDeleteChatApiFailure(response?.body()?.meta?.message.toString(), true)
                }
            }

            override fun onFailure(call: Call<DeleteChatDataResponse>?, t: Throwable?) {
                mDeleteChatListner.onDeleteChatApiFailure(t?.message.toString(), true)
            }
        })
    }
}