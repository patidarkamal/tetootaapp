package com.tetoota.fragment.settings

data class UserVisibilityDataResponse(
	val is_visible: Int? = null
)
