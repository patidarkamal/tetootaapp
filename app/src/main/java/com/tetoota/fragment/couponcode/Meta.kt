package com.tetoota.fragment.couponcode

data class Meta(
	val code: Int? = null,
	val message: String? = null,
	val status: Boolean? = null
)