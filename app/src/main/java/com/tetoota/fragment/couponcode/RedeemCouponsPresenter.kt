package com.tetoota.fragment.couponcode

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class RedeemCouponsPresenter : RedeemCouponsContract.Presenter, RedeemCouponsContract.RedeemCouponsApiResult{
    var mFavoriteView : RedeemCouponsContract.View

    val mFavoriteInteractor : RedeemCouponsInteractor by lazy {
        RedeemCouponsInteractor(this)
    }

    constructor(mFavoriteView: RedeemCouponsContract.View) {
        this.mFavoriteView = mFavoriteView
    }

    override fun getRedeemCouponsCode(mActivity : Activity, mReferralCode : String) {
        mFavoriteInteractor.getRedeemCoupons(mActivity, mReferralCode)
    }

    override fun onRedeemCouponsApiSuccess(mString: String, mReferralCode : String) {
        mFavoriteView.onRedeemCouponsSuccess(mString, mReferralCode)
    }

    override fun onRedeemCouponsApiFailure(mString: String, isServerError: Boolean) {
        mFavoriteView.onRedeemCouponsFailureResponse(mString,isServerError)
    }
}