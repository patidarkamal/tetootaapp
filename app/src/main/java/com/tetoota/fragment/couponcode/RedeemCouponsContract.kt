package com.tetoota.fragment.couponcode

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class RedeemCouponsContract {
    interface View {
        fun onRedeemCouponsSuccess(mString: String, mFavoriteList: String): Unit
        fun onRedeemCouponsFailureResponse(mString: String, isServerError: Boolean): Unit
    }

    interface Presenter {
        fun getRedeemCouponsCode(mActivity: Activity, mReferralCode : String)
    }

    interface RedeemCouponsApiResult {
        fun onRedeemCouponsApiSuccess(mString: String, mReferralCode: String): Unit
        fun onRedeemCouponsApiFailure(mString: String, isServerError: Boolean): Unit
    }
}