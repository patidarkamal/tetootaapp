package com.tetoota.fragment.couponcode

import android.app.Activity
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.fragment.couponcode.coupon_code.CouponCodeDataResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException


/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class RedeemCouponsInteractor {
    var call: Call<CouponCodeDataResponse>? = null
    var mInviteFriendsListener: RedeemCouponsContract.RedeemCouponsApiResult

    constructor(mFavoriteListener: RedeemCouponsContract.RedeemCouponsApiResult) {
        this.mInviteFriendsListener = mFavoriteListener
    }

    fun getRedeemCoupons(mActivity: Activity, mReferralCode: String) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        call = TetootaApplication.getHeader()
                .getCouponsCode(
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),
                        personData.user_id!!,
                        mReferralCode)
        call!!.enqueue(object : Callback<CouponCodeDataResponse> {
            override fun onResponse(call: Call<CouponCodeDataResponse>?,
                                    response: Response<CouponCodeDataResponse>?) {
                var mRedeemCouponsData: CouponCodeDataResponse? = response?.body()
                if (response?.code() == 200) {
                    if (response.body()?.meta?.status!!) {
                        if (mRedeemCouponsData != null) {
                            mInviteFriendsListener.onRedeemCouponsApiSuccess("success", mRedeemCouponsData.meta!!.message as String)
                        }
                    } else {
                        mInviteFriendsListener.onRedeemCouponsApiFailure(response?.body()?.meta?.message.toString(), false)
                    }
                } else {
                    if (response?.body() == null) {
                        try {
                            val jObjError = JSONObject(response?.errorBody()?.string())
                            val achualdata: JSONObject = jObjError.getJSONObject("meta")
                            val gson = Gson()
                            var mError: Meta = Meta()
                            val error = gson.fromJson(achualdata.toString(), Meta::class.java)
                            mInviteFriendsListener.onRedeemCouponsApiFailure(error.message!!, true)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        /* var mRedeemCouponsData : ResponseBody? = response?.errorBody()
                         val gson = GsonBuilder().create()
                         var mError = Meta()
                         try {
                           var  mError: String? = mRedeemCouponsData.
                             Log.d("nik",mError.toString());
                         } catch (e: IOException) {
                             // handle failure to read error
                             e.printStackTrace();
                         }*/
                    }
                }
            }

            override fun onFailure(call: Call<CouponCodeDataResponse>?, t: Throwable?) {
                mInviteFriendsListener.onRedeemCouponsApiFailure(t?.message.toString(), true)
            }
        })
    }
}