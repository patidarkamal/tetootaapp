package com.tetoota.fragment.invite_friends

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class InviteFriendsPresenter  : InviteFriendsContract.Presenter, InviteFriendsContract.ReferralCodeApiResult{
    var mFavoriteView : InviteFriendsContract.View

    val mFavoriteInteractor : InviteFriendsInteractor by lazy {
        InviteFriendsInteractor(this)
    }

    constructor(mFavoriteView: InviteFriendsContract.View) {
        this.mFavoriteView = mFavoriteView
    }

    override fun getUserFavoriteList(mActivity : Activity) {
        mFavoriteInteractor.getREferralCode(mActivity)
    }

    override fun cancelRequest(mActivity: Activity,wishListType : String) {
        mFavoriteInteractor.cancelREquest(mActivity,wishListType)
    }


    override fun onReferralCodeApiSuccess(mString: String, mReferralCode : String) {
        mFavoriteView.onReferralSuccess(mString, mReferralCode)
    }

    override fun onReferralCodeApiFailure(mString: String, isServerError: Boolean) {
        mFavoriteView.onReferralFailureResponse(mString,isServerError)
    }
}