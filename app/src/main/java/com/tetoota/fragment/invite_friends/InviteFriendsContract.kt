package com.tetoota.fragment.invite_friends

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class InviteFriendsContract {
    interface View{
        fun onReferralSuccess(mString: String, mFavoriteList: String) : Unit
        fun onReferralFailureResponse(mString: String,isServerError: Boolean) : Unit
    }

    interface Presenter{
        fun getUserFavoriteList(mActivity : Activity)
        fun cancelRequest(mActivity: Activity,wishListType : String)
    }

    interface ReferralCodeApiResult{
        fun onReferralCodeApiSuccess(mString : String, mReferralCode : String) : Unit
        fun onReferralCodeApiFailure(mString : String, isServerError: Boolean) : Unit
    }
}