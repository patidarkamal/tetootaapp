package com.tetoota.fragment.invite_friends

data class Meta(
	val code: Int? = null,
	val message: String? = null,
	val status: Boolean? = null
)
