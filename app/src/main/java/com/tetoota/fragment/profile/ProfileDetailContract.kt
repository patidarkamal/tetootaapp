package com.tetoota.fragment.profile

import android.app.Activity
import android.app.Service
import com.tetoota.login.LoginDataResponse
import okhttp3.RequestBody

/**
 * Created by charchit.kasliwal on 18-07-2017.
 */
class ProfileDetailContract {
    interface View {
        fun onProfileApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String)
        fun onProfileApiFailureResult(message: String, apiCallMethod: String)
        fun onShowUserApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String)
        fun onShowUserApiFailureResult(message: String, apiCallMethod: String)
    }

    internal interface Presenter {
        fun getUserProfileData(mActivity: Activity, personData: LoginDataResponse)
        fun getUserProfileDataForService(mService: Service, personData: LoginDataResponse)
        fun getUserDetailShow(mActivity: Activity, mUserId : String)
        fun updateUserProfileData(mActivity: Activity, data: HashMap<String, RequestBody>, mImagePath: String?)
    }

    interface profileApiListener {
        fun onProfileApISuccess(profileData: List<ProfileDataResponse?>?, message: String)
        fun onProfileApiFailure(message: String, apiCallMethod: String)
        fun onShowUserApISuccess(profileData: List<ProfileDataResponse?>?, message: String)
        fun onShowUserApiFailure(message: String, apiCallMethod: String)

    }
}