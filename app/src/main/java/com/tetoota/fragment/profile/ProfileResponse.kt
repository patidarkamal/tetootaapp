package com.tetoota.fragment.profile
import com.tetoota.network.errorModel.Meta
data class ProfileResponse(
		val data: List<ProfileDataResponse?>? = null,
		val meta: Meta? = null
)
