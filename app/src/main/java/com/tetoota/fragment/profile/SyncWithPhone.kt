package com.tetoota.fragment.profile

data class SyncWithPhone(
	val whatsapp: Boolean? = null,
	val gmail: Boolean? = null,
	val facebook: Boolean? = null,
	val linkedin: Boolean? = null,
	val instagram: Boolean? = null
)
