package com.tetoota.fragment

import android.app.Activity
import android.app.ProgressDialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import com.tetoota.R

/**
 * Created by charchit.kasliwal on 09-06-2017.
 */
open class BaseFragment : Fragment(){
    var mProgressDialog : ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mProgressDialog = ProgressDialog(activity, R.style.TransparentProgressDialog)
    }
    fun showSnackBar(message: String, view: View?) {
        val snackbar = Snackbar.make(view!!.findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT)
        val sbView = snackbar.view
        val textView = sbView
                .findViewById<TextView>(R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(this!!.activity!!, R.color.color_white))
        snackbar.show()
    }

    fun showProgressDialog(mText : String) {
        mProgressDialog?.setCancelable(false)
        mProgressDialog?.setMessage(mText)
        mProgressDialog?.show()
    }

    fun hideProgressDialog() {
        if(mProgressDialog != null && mProgressDialog?.isShowing!!){
            mProgressDialog?.dismiss()
        }
    }

    fun removeDialog() {
        mProgressDialog?.dismiss()
    }

    /**
     * Method to Disable Progress bar Touch
     */
    fun disableWindowTouch(mActivity : Activity){
        mActivity.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    /**
     * Method to Enable Progress bar Touch
     */
    fun enableWindowTouch(mActivity: Activity){
        mActivity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }
}