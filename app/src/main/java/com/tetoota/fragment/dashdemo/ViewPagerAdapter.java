package com.tetoota.fragment.dashdemo;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.tetoota.fragment.home.HomeFragment;
import com.tetoota.fragment.nearby.NearByFragment;
import com.tetoota.service_product.ProductFragment;
import com.tetoota.service_product.ServiceFragment;
import com.tetoota.service_product.WishlistFragment;
import com.tetoota.utility.StringConstant;
import com.tetoota.utility.Utils;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private int NoOfTabs;
    private HomeFragment.OnChildFragmentInteractionListener iChildClickListener;

    private HomeFragment fragmentHome;
    private ServiceFragment fragmentService;
    private ProductFragment fragmentProduct;
    private WishlistFragment fragmentWishlist;
    private NearByFragment fragmentNearBy;
    private Context mContext;

    public ViewPagerAdapter(Context context,int NoOfTabs, FragmentManager fm , HomeFragment.OnChildFragmentInteractionListener iChildClickListener) {
        super(fm);
        this.NoOfTabs = NoOfTabs;
        this.iChildClickListener = iChildClickListener;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return NoOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
      //  Fragment fragment = null;

        if (position == 0)
        {
            fragmentHome = new HomeFragment();
            fragmentHome.setmListener(iChildClickListener);
            return fragmentHome;

        }
        else if (position == 1)
        {
            fragmentService = new ServiceFragment();
            return fragmentService;

        }
        else if (position == 2)
        {
            fragmentProduct = new ProductFragment();
            return fragmentProduct;

        }
        else if (position == 3)
        {
            fragmentWishlist = new WishlistFragment();
            return fragmentWishlist;

        }
        else if (position == 4)
        {
            fragmentNearBy = new NearByFragment();
            return fragmentNearBy;

        }
        return null;
    }

   @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
      // Drawable myDrawable = null;
        if (position == 0)
        {
            title = Utils.Utils.getText(mContext, StringConstant.util.getHome());
           // title = "Home1";
        }
        else if (position == 1)
        {
            title = Utils.Utils.getText(mContext, StringConstant.util.getServices());
           // title = "Services1";

        }
        else if (position == 2)
        {
            title = Utils.Utils.getText(mContext, StringConstant.util.getProduct());
            //title = "Products1";

        }
        else if (position == 3)
        {
            title = Utils.Utils.getText(mContext, StringConstant.util.getWishlist());
           // title = "Wishlist1";

        }

        else if (position == 4)
        {
            title = Utils.Utils.getText(mContext, StringConstant.util.getNear_by());
            //title = "Nearby1";

        }
        return title;
    }

    interface OnFilterAppliedListener {
        void filterData(String mTopRated, String mCity);
    }

    void updateFreagment(){
        this.notifyDataSetChanged();
    }

}

