package com.tetoota.fragment.help.di

import android.app.Activity
import com.tetoota.fragment.help.data.DataItem

/**
 * Created by abhinav.maurya on 01-02-2018.
 */
class ShowWebViewContractor {

    interface View{
        fun onPageDetailsAPiSuccess(mString: String, pageDetailResponse: ArrayList<DataItem>)
        fun onPageDetailsAPiFailure(mString: String,isServerError: Boolean)
    }

    interface Presenter{
        fun PageDetailsData(mActivity : Activity, page_title : String)
    }

    interface PageDetailsResult{
        fun onPageDetailsAPiSuccess(mString: String, pageDetailResponse: ArrayList<DataItem>) : Unit
        fun onPageDetailsAPiFailure(mString : String, isServerError: Boolean) : Unit
    }
}