package com.tetoota.fragment.help.data

import com.google.gson.annotations.SerializedName
import com.tetoota.network.errorModel.Meta

data class ShowWebViewResponse(

	@field:SerializedName("data")
	val data: ArrayList<DataItem>? = null,

	@field:SerializedName("meta")
	val meta: Meta? = null
)