package com.tetoota.fragment.help.data

import android.app.Activity
import com.tetoota.TetootaApplication
import com.tetoota.fragment.help.di.ShowWebViewContractor
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by abhinav.maurya on 01-02-2018.
 */
class ShowWebViewInteractor {

    var call: Call<ShowWebViewResponse>? = null
    var mShowWebViewListener: ShowWebViewContractor.PageDetailsResult

    constructor(mShowWebViewListener: ShowWebViewContractor.PageDetailsResult) {
        this.mShowWebViewListener = mShowWebViewListener
    }

    fun getUserHoldPoints(mActivity: Activity, page_title: String) {
        call = TetootaApplication.getHeader()
                .getPageDetails(Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity)
                        , page_title)
        call!!.enqueue(object : Callback<ShowWebViewResponse> {
            override fun onResponse(call: Call<ShowWebViewResponse>?,
                                    response: Response<ShowWebViewResponse>?) {
                var mShowWebViewResponse: ShowWebViewResponse? = response?.body()
                if (response?.code() == 200) {
                    if (mShowWebViewResponse != null && mShowWebViewResponse.data?.size!! > 0) {
                        mShowWebViewListener.onPageDetailsAPiSuccess("success", mShowWebViewResponse.data!!)
                    } else {
                        mShowWebViewListener.onPageDetailsAPiFailure("failure", false)
                    }
                } else {
                }
            }

            override fun onFailure(call: Call<ShowWebViewResponse>?, t: Throwable?) {
                mShowWebViewListener.onPageDetailsAPiFailure(t?.message.toString(), false)
            }
        })
    }

}