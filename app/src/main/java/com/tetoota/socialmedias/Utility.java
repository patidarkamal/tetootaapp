package com.tetoota.socialmedias;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by abhinav.maurya on 30-06-2017.
 */

public class Utility {

    /**
     * Save the value in preference
     *
     * @param key
     * @param value
     * @param activity
     */
    public static void savePreferences(String key, Object value,
                                       Context activity) {
        SharedPreferences sharedPreferences = activity.getApplicationContext().getSharedPreferences(
                SocialMediaConstant.SOCIAL_MEDIA_PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if(value instanceof String){
            editor.putString(key, (String) value);
        }else if(value instanceof Boolean){
            editor.putBoolean(key, (Boolean) value);
        }else if(value instanceof Integer){
            editor.putInt(key, (Integer) value);
        }
        editor.commit();
    }

    /**
     * get the value from preference
     *
     * @param key
     * @param defaultStr
     * @param activity
     * @return
     */
    public static String loadPrefrence(String key, String defaultStr,
                                       Context activity) {
        SharedPreferences sharedPreferences = activity.getApplicationContext().getSharedPreferences(
                SocialMediaConstant.SOCIAL_MEDIA_PREFERENCE_NAME, 0);
        return sharedPreferences.getString(key, defaultStr);
    }

    /**
     * get the value from preference
     *
     * @param key
     * @param defaultBoolean
     * @param activity
     * @return
     */
    public static Boolean loadPrefrence(String key, boolean defaultBoolean,
                                        Context activity) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(activity);
        return sharedPreferences.getBoolean(key, defaultBoolean);
    }

    public static void clearAllPrefrences(Context activity) {
        SharedPreferences sharedPreferences = activity.getApplicationContext().getSharedPreferences(
                SocialMediaConstant.SOCIAL_MEDIA_PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }
}
