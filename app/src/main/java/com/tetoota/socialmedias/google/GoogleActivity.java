package com.tetoota.socialmedias.google;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.tetoota.R;
import com.tetoota.socialmedias.AlertDialogManager;
import com.tetoota.socialmedias.SocialMediaConstant;
import com.tetoota.socialmedias.SocialMediaProfilePojo;
import com.tetoota.socialmedias.Utility;


/**
 * Google SignIn credentials are created by
 * UserId: 2610pratik@gmail.com
 * Password: ht123456
 */
public class GoogleActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private GoogleApiClient mGoogleApiClient = null;
    private static final int RC_SIGN_IN = 9001;
    private String type = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_activity);

        type = getIntent().getStringExtra("extra");
        googleIntegration();
        mGoogleApiClient.connect();
        mGoogleApiClient.registerConnectionCallbacks(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void googleIntegration() {
        // [START configure_signin]
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(GoogleActivity.this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        // [END build_client]
    }

    private void checkPreviousLoginDetail() {
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
//            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
//                @Override
//                public void onResult(GoogleSignInResult googleSignInResult) {
//                    handleSignInResult(googleSignInResult);
//                }
//            });
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Intent intent = new Intent();
            SocialMediaProfilePojo socialMediaProfilePojo = new SocialMediaProfilePojo();
            Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_IS_LOGIN_KEY, true, GoogleActivity.this);

            socialMediaProfilePojo.setSocialMediaType(SocialMediaConstant.GOOGLE);
            Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_TYPE_KEY,
                    SocialMediaConstant.GOOGLE, GoogleActivity.this);

            String profileId = acct.getId();
            if (null != profileId) {
                socialMediaProfilePojo.setProfileId(profileId);
                Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_USER_ID_KEY, profileId, GoogleActivity.this);
            }

            String userName = acct.getDisplayName();
            if (null != userName) {
                socialMediaProfilePojo.setUserName(userName);
                Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_USER_NAME_KEY, userName, GoogleActivity.this);
            }

            if (null != acct.getPhotoUrl()) {
                String profileImage = acct.getPhotoUrl().toString();
                socialMediaProfilePojo.setProfileImage(profileImage);
                Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_PROFILE_IMAGE_KEY, profileImage, GoogleActivity.this);
            }

            String email = acct.getEmail();
            if (null != email) {
                socialMediaProfilePojo.setEmailAddress(email);
                Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_EMAIL_ADDRESS_KEY, email, GoogleActivity.this);
            }

            intent.putExtra("result", socialMediaProfilePojo);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }

    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            setResult(RESULT_OK);
                            finish();
                        } else {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        new AlertDialogManager().showAlertDialog(GoogleActivity.this, "Error",
                connectionResult.getErrorMessage(), false, true);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (type.equalsIgnoreCase("login")) {
            ((TextView) findViewById(R.id.messagetext)).setText(getString(R.string.authenticating));
            checkPreviousLoginDetail();
        } else {
            ((TextView) findViewById(R.id.messagetext)).setText(getString(R.string.logout_every_media));
            signOut();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
