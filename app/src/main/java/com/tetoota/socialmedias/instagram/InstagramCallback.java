package com.tetoota.socialmedias.instagram;


import com.tetoota.socialmedias.SocialMediaProfilePojo;

/**
 * Created by abhinav.maurya on 03-07-2017.
 */

public interface InstagramCallback {

    public void returnLoginDetails(SocialMediaProfilePojo socialMediaProfilePojo);

}
