package com.tetoota.socialmedias;

/**
 * Created by abhinav.maurya on 29-06-2017.
 */

public class SocialMediaConstant {

    // Preference Key
    public static final String SOCIAL_MEDIA_PREFERENCE_NAME = "social_media";
    public static final String SOCIAL_MEDIA_TYPE_KEY = "type";
    public static final String SOCIAL_MEDIA_IS_LOGIN_KEY = "islogin";
    public static final String SOCIAL_MEDIA_USER_ID_KEY = "id";
    public static final String SOCIAL_MEDIA_USER_NAME_KEY = "name";
    public static final String SOCIAL_MEDIA_PROFILE_IMAGE_KEY = "image";
    public static final String SOCIAL_MEDIA_EMAIL_ADDRESS_KEY = "email";

    // Social media type used in login
    public static final String FACEBOOK = "1";
    public static final String TWITTER = "2";
    public static final String LINKEDIN = "3";
    public static final String GOOGLE = "4";
    public static final String INSTRAGRAM = "5";


      // Insta Client Id : 780hcdbl8n217w
    // LINED : CLIENT SECRET : dkaaQGwF8fTv5twk
    // LinkedIn constant
    private static final String LINKEDIN_HOST_URL = "api.linkedin.com";
    public static final String LINKEDIN_PERMISSIONS_URL = "https://" + LINKEDIN_HOST_URL
            + "/v1/people/~:(id,email-address,formatted-name,phone-numbers,public-profile-url,picture-url,picture-urls::(original))";
    public static final String LINKEDIN_SHARE_URL = "https://api.linkedin.com/v1/people/~/shares";

    //keytool -exportcert -keystore E:\Android\workspace_kotlin\branches\Tetoota\tetoota.jks -alias tetoota | openssl sha1 -binary | openssl base64

    // Instagram constant
    public static final String instagram_client_iD = "f03ad3c659604b8bb387d87604a77c89";
    public static final String instagram_client_secret = "e4eabaeabc1d442ba23b860c5a036e3b";
    public static final String instagram_callback_url = "https://www.hiteshi.com/";

    // Places location to map image
    public static final String LOCATION_TO_MAP_IMAGE_URL = "https://maps.googleapis.com/maps/api/staticmap?center=";// add location lat/long after it
    public static final String LOCATION_TO_MAP_IMAGE_SIZE_TAG = "&size=";// add size axb
    public static final String LOCATION_TO_MAP_IMAGE_MARKER_TAG = "&markers=size:mid|color:red|";// add location lat/long for the marker
    public static final String PLACES_API = "https://maps.googleapis.com/maps/api/staticmap?center=22.6843324197085,75.87086201970848&size=400x400&markers=size:mid|color:red|22.6843324197085,75.87086201970848";

}
