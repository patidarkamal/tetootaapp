package com.tetoota.socialmedias.facebook;

/**
 * Created by jitendra.nandiya on 22-11-2017.
 */

public interface AlertDialogCallBack {
    public void onSuccess (String id, Object object);
    public void onCancel();
}
