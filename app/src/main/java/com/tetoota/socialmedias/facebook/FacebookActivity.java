package com.tetoota.socialmedias.facebook;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.model.ShareVideo;
import com.facebook.share.model.ShareVideoContent;
import com.tetoota.R;
import com.tetoota.customviews.AlertDialogManager;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jitendra.nandiya on 22-11-2017.
 */

public class FacebookActivity extends Activity implements AlertDialogCallBack{

    // Alert Dialog Manager
    AlertDialogManager alert = new AlertDialogManager();
    private CallbackManager callbackManager;
    private LoginManager loginManager;
    private LoginButton login;
    private final int sharedText = 1;
    private final int shareImageText = 2;
    private final int shareVideoText = 3;
    private String shareText = "";
    private int shareType = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_facebook);

        callbackManager = CallbackManager.Factory.create();
        login = (LoginButton) findViewById(R.id.facebook_login_button);

        login.setReadPermissions("public_profile,email,user_friends,publish_actions");

        String value = getIntent().getStringExtra("extra");
        shareText = getIntent().getStringExtra("sharetext");
        shareType = getIntent().getIntExtra("sharetype", 1);

        // Just to login into facebook
        if (value.equalsIgnoreCase("login")) {

            ((TextView) findViewById(R.id.messagetext)).setText(getString(R.string.authenticating));
            getKeyHash();

            if (AccessToken.getCurrentAccessToken() != null) {
                RequestData();
            } else {
                login.performClick();
            }

            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                     Log.d("view ","");
                }
            });

            login.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        RequestData();
                    }
                }

                @Override
                public void onCancel() {
                    Log.d("onCancel ","");
                }

                @Override
                public void onError(FacebookException exception) {
                    if (null != exception.getCause()) {
                        alert.showAlertDialog(FacebookActivity.this, "Error",
                                exception.getCause().toString(), false, true, FacebookActivity.this, "0");
                    } else {
                        alert.showAlertDialog(FacebookActivity.this, "Error",
                                "Connection Error", false, true, FacebookActivity.this, "0");
                    }
                }
            });
        } else if (value.equalsIgnoreCase("logout")) { // Just to logout from facebook
            ((TextView) findViewById(R.id.messagetext)).setText(getString(R.string.logout_every_media));
            logoutFacebook();
        } else if (value.equalsIgnoreCase("share")) { // Share into facebook after login which can be later done
//            shareToFacebook(shareType, shareText, shareImage, shareVideoUri);
        } else if (value.equalsIgnoreCase("loginandshare")) { // Share immediately after login
            ((TextView) findViewById(R.id.messagetext)).setText(getString(R.string.authenticating));
            callbackManager = CallbackManager.Factory.create();

            List<String> permissionNeeds = Arrays.asList("publish_actions");

            //this loginManager helps you eliminate adding a LoginButton to your UI
            loginManager = LoginManager.getInstance();

            loginManager.logInWithPublishPermissions(this, permissionNeeds);

            loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    ((TextView) findViewById(R.id.messagetext)).setText(getString(R.string.share_input));
                    shareToFacebook(shareType, shareText);
                }

                @Override
                public void onCancel() {
                    setResultAndFinish(RESULT_CANCELED);
                }

                @Override
                public void onError(FacebookException exception) {
                    setResultAndFinish(RESULT_CANCELED);
                }
            });
        }
    }

    private void getKeyHash() {
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.tetoota", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                JSONObject json = response.getJSONObject();
                System.out.println("Json data :" + json);
                if (json != null) {
//                    Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_IS_LOGIN_KEY, true, FacebookActivity.this);
                    finish();
                } else {
                    alert.showAlertDialog(FacebookActivity.this, "Error",
                            "Connection Error", false, true, FacebookActivity.this, "0");
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void logoutFacebook() {
        GraphRequest delPermRequest = new GraphRequest(AccessToken.getCurrentAccessToken(),
                "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                if (graphResponse != null) {
                    FacebookRequestError error = graphResponse.getError();
                    if (error != null) {
                        setResultAndFinish(RESULT_CANCELED);
                    } else {
                        LoginManager.getInstance().logOut();
                        setResultAndFinish(RESULT_OK);
                    }
                }
            }
        });
        delPermRequest.executeAsync();
    }

    private void shareToFacebook(int shareType, String textToShare) {
        Bitmap myBitmap = null;
//        if(imageString != null) {
//            File imgFile = new File(imageString);
//            myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//        }

        Uri uri = null;
//        if(videoUri != null) {
//            uri = Uri.parse(videoUri);
//        }
        switch (shareType){
            case sharedText:
                Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.iv_tetoota_icon);
                SharePhoto photoText = new SharePhoto.Builder()
                        .setBitmap(image)
                        .setCaption(textToShare)
                        .build();
                SharePhotoContent sharePhotoContentText = new SharePhotoContent.Builder()
                        .addPhoto(photoText)
                        .build();
                ShareApi.share(sharePhotoContentText, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        setResultAndFinish(RESULT_OK);
                    }

                    @Override
                    public void onCancel() {
                        setResultAndFinish(RESULT_CANCELED);
                    }

                    @Override
                    public void onError(FacebookException error) {
                        setResultAndFinish(RESULT_CANCELED);
                    }
                });
                break;
            case shareImageText:
                SharePhoto photo = new SharePhoto.Builder()
                        .setBitmap(myBitmap)
                        .setCaption(textToShare)
                        .build();
                SharePhotoContent sharePhotoContent = new SharePhotoContent.Builder()
                        .addPhoto(photo)
                        .build();
                ShareApi.share(sharePhotoContent, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        setResultAndFinish(RESULT_OK);
                    }

                    @Override
                    public void onCancel() {
                        setResultAndFinish(RESULT_CANCELED);
                    }

                    @Override
                    public void onError(FacebookException error) {
                        setResultAndFinish(RESULT_CANCELED);
                    }
                });
                break;
            case shareVideoText:
                ShareVideo video = new ShareVideo.Builder()
                        .setLocalUrl(uri)
                        .build();
                ShareVideoContent shareVideoContent = new ShareVideoContent.Builder()
                        .setVideo(video)
                        .build();
                ShareApi.share(shareVideoContent, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        setResultAndFinish(RESULT_OK);
                    }

                    @Override
                    public void onCancel() {
                        setResultAndFinish(RESULT_CANCELED);
                    }

                    @Override
                    public void onError(FacebookException error) {
                        setResultAndFinish(RESULT_CANCELED);
                    }
                });
                break;
        }
    }

    private void setResultAndFinish(int resultType){
        setResult(resultType);
        finish();
    }

    @Override
    public void onSuccess(String id, Object object) {

    }

    @Override
    public void onCancel() {

    }
}