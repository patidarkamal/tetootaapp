package com.tetoota.socialmedias;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.tetoota.R;


public class AlertDialogManager {
    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     *               - pass null if you don't want icon
     * */
    public void showAlertDialog(final Activity context, String title, String message,
                                Boolean status, final boolean isFinishing) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        if(status != null)
            // Setting alert dialog icon
            alertDialog.setIcon((status) ? R.mipmap.twitter_icon : R.mipmap.ic_launcher);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(isFinishing){
                    context.setResult(context.RESULT_CANCELED);
                    context.finish();
                }
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
}
